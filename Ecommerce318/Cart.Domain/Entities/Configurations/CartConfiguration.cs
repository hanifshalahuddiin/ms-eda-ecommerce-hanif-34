﻿using Microsoft.EntityFrameworkCore; //ToTable, IEntityTypeConfiguration
using Microsoft.EntityFrameworkCore.Metadata.Builders; //EntityTypeBuilder,

namespace Cart.Domain.Entities.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<UserEntity>
    {
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.ToTable("Users");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.UserName).IsRequired();
            builder.Property(e => e.Email).IsRequired();
            builder.Property(e => e.Status).IsRequired();
            builder.Property(e => e.Type).IsRequired();
        }
    }

    public class CartConfiguration : IEntityTypeConfiguration<CartEntity>
    {
        public void Configure(EntityTypeBuilder<CartEntity> builder)
        {
            builder.ToTable("Carts");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.CustomerId).IsRequired();
            builder.Property(e => e.CartStatus).IsRequired();
            builder.Property(e => e.ProductStatus).IsRequired();
        }
    }

    /*
        public class CartProductConfiguration : IEntityTypeConfiguration<CartProductEntity>
        {
            public void Configure(EntityTypeBuilder<CartProductEntity> builder)
            {
                builder.ToTable("CartProducts");
                builder.HasKey(e => e.Id);
                builder.Property(e => e.CartId).IsRequired();
                builder.Property(e => e.ProductId).IsRequired();
                builder.Property(e => e.SKU).IsRequired();
                builder.Property(e => e.Name).IsRequired();
                builder.Property(e => e.Quantity).IsRequired();
                builder.Property(e => e.Price);
            }
        }
    */
}