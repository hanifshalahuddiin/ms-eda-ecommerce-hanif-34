﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Cart.Domain.Entities
{
    public class CartEntity
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public CartStatusEnum CartStatus { get; set; } = default!;
        public ProductStatusEnum ProductStatus { get; set; } = default!;
        public DateTime Modified { get; internal set; } = DateTime.Now;

        [ForeignKey("CustomerId")]
        public virtual UserEntity Customer { get; set; }
    }
}
