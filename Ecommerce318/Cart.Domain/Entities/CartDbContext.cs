﻿using Microsoft.EntityFrameworkCore;
using Cart.Domain.Entities.Configurations;
using Microsoft.Extensions.Configuration;

namespace Cart.Domain.Entities
{
    public class CartDbContext : DbContext
    {
        public CartDbContext(DbContextOptions<CartDbContext> options) : base(options)
        {

        }

        public DbSet<UserEntity> Users { get; set; }
        public DbSet<CartEntity> Carts { get; set; }
        //public DbSet<ProductEntity> Products { get; set; }
        //public DbSet<CartProductsEntity> CartProducts { get; set; }
        //public DbSet<UserEntity> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new CartConfiguration());
            //modelBuilder.ApplyConfiguration(new ProductConfiguration());
            //modelBuilder.ApplyConfiguration(new CartProductsConfiguration());
            //modelBuilder.ApplyConfiguration(new UserConfiguration());
        }

        public static DbContextOptions<CartDbContext> OnConfigure()
        {
            var optionsBuilder = new DbContextOptionsBuilder<CartDbContext>();
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            optionsBuilder.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("Store_Db_Conn").Value);

            return optionsBuilder.Options;
        }
    }
}
