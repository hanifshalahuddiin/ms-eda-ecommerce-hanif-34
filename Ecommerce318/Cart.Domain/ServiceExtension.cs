﻿using Cart.Domain.Entities;
using Microsoft.EntityFrameworkCore;//DbContextOptionBuilder
using Microsoft.Extensions.DependencyInjection;//IServiceCollection

namespace Cart.Domain
{
    public static class ServiceExtension
    {
        public static void AddDomainContext(this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionAction,
            ServiceLifetime contextLifetime = ServiceLifetime.Scoped,
            ServiceLifetime optionLifetime = ServiceLifetime.Scoped)
        {
            services.AddDbContext<CartDbContext>(optionAction, contextLifetime, optionLifetime);
        }
    }
}
