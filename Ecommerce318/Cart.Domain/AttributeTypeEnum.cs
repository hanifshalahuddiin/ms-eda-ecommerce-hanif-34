﻿using System.Text.Json.Serialization;

namespace Cart.Domain
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum AttributeTypeEnum
    {
        Text,
        Number,
        Decimal
    }
}
