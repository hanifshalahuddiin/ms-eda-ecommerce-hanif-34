﻿using Cart.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Cart.Domain.Repositories
{
    public interface IUserRepository
    {
        //Task<int> GetCount();
        Task<IEnumerable<UserEntity>> GetAll();
        //Task<IEnumerable<UserEntity>> GetPaged(int page, int size);
        Task<UserEntity> GetById(Guid id);
        //Task<UserEntity> Add(UserEntity entity);
        //Task<UserEntity> Update(UserEntity entity);
        //Task<UserEntity> Delete(UserEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);


        //Task<UserEntity> Login(string userName, string password);
    }

    public class UserRepository : IUserRepository
    {
        protected readonly CartDbContext _context;

        public UserRepository(CartDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<IEnumerable<UserEntity>> GetAll()
        {
            var result = await _context.Set<UserEntity>().ToListAsync();
            return result;
        }

        public async Task<UserEntity> GetById(Guid id)
        {
            var result = await _context.Set<UserEntity>().FindAsync(id);
            return result;
        }

        //public async Task<UserEntity> Add(UserEntity entity)
        //{
        //    var result = _context.Set<UserEntity>().Add(entity);
        //    return entity;
        //}

        //public async Task<UserEntity> Update(UserEntity entity)
        //{
        //    _context.Set<UserEntity>().Update(entity);
        //    return entity;
        //}

        //public async Task<UserEntity> Delete(UserEntity entity)
        //{
        //    _context.Set<UserEntity>().Update(entity);
        //    return entity;
        //}

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            var result = await _context.SaveChangesAsync(cancellationToken);
            return result;
        }

        //public Task<int> GetCount()
        //{
        //    throw new NotImplementedException();
        //}

        //public Task<IEnumerable<UserEntity>> GetPaged(int page, int size)
        //{
        //    throw new NotImplementedException();
        //}

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
