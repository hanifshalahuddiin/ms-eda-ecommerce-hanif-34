﻿using Cart.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Cart.Domain.Repositories
{
    public interface ICartRepository
    {
        Task<int> GetCount();
        Task<IEnumerable<CartEntity>> GetAll();
        Task<IEnumerable<CartEntity>> GetPaged(int page, int size);
        Task<CartEntity> GetById(Guid id);
        Task<CartEntity> Add(CartEntity entity);
        Task<CartEntity> Update(CartEntity entity);
        void Delete(CartEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }

    public class CartRepository : ICartRepository
    {
        protected readonly CartDbContext _context;

        public CartRepository(CartDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<CartEntity> Add(CartEntity entity)
        {
            //throw new NotImplementedException();
            var result = _context.Set<CartEntity>().Add(entity);
            return entity;
        }

        public void Delete(CartEntity entity)
        {
            //throw new NotImplementedException();
        }

        public async Task<IEnumerable<CartEntity>> GetAll()
        {
            //throw new NotImplementedException();
            var result = await _context.Set<CartEntity>().ToListAsync();
            return result;
        }

        public async Task<CartEntity> GetById(Guid id)
        {
            //throw new NotImplementedException();
            var result = await _context.Set<CartEntity>().FindAsync(id);
            return result;
        }

        public Task<int> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<CartEntity>> GetPaged(int page, int size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            //throw new NotImplementedException();
            var result = await _context.SaveChangesAsync(cancellationToken);
            return result;
        }

        public async Task<CartEntity> Update(CartEntity entity)
        {
            //throw new NotImplementedException();
            _context.Set<CartEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
