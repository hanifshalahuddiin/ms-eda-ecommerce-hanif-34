﻿using Framework.Core.Projection;
using Microsoft.Extensions.DependencyInjection;
using Cart.Domain.Projections;

namespace Cart.Domain
{
    public static class StoreServices
    {
        public static IServiceCollection AddFromUser(this IServiceCollection services) =>
            services.AddUserProjection();
        //public static IServiceCollection AddFromStore(this IServiceCollection services) =>
        //    services.AddStoreProjection();

        //private static IServiceCollection AddStoreProjection(this IServiceCollection services) =>
        //    services //.AddScoped<AttributeProjection>();
        //    .Projection(builder => builder
        //        .AddOn<ProductCreated>(ProductProjection.Handle)
        //        .AddOn<ProductUpdated>(ProductProjection.Handle)
        //        .AddOn<ProductStatusChanged>(ProductProjection.Handle)

        //    //.AddOn<CurrencyCreated>(CurrencyProjection.Handle)
        //    );

        private static IServiceCollection AddUserProjection(this IServiceCollection services) =>
            services //.AddScoped<AttributeProjection>();
            .Projection(builder => builder
                .AddOn<UserCreated>(UserProjection.Handle)
                .AddOn<UserUpdated>(UserProjection.Handle)
                .AddOn<UserStatusChanged>(UserProjection.Handle)
            );
    }
}
