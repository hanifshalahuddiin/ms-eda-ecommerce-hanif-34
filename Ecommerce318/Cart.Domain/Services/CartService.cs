﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Entities;
using Cart.Domain.Repositories;

namespace Cart.Domain.Services
{
    //internal class UserService
    //{
    //}

    public interface ICartService
    {
        Task<IEnumerable<CartDto>> All();
        Task<CartDto> GetCartById(Guid id);
        Task<CartDto> AddCart(CartDto dto);
        Task<bool> UpdateCart(CartDto dto);
        Task<bool> UpdateCartStatus(Guid id, CartStatusEnum status);
    }

    public class CartService : ICartService
    {
        private ICartRepository _repository;
        private readonly IMapper _mapper;

        public async Task<IEnumerable<CartDto>> All()
        {
            //throw new NotImplementedException();
            var result = _mapper.Map<IEnumerable<CartDto>>(await _repository.GetAll());
            return result;
        }

        public async Task<CartDto> GetCartById(Guid id)
        {
            //throw new NotImplementedException();
            if (id != null)
            {
                var result = _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<CartDto>(result);
                }
            }
            return null;
        }

        public async Task<CartDto> AddCart(CartDto dto)
        {
            //throw new NotImplementedException();
            if (dto != null)
            {
                dto.CartStatus = CartStatusEnum.Pending;
                var dtoToEntity = _mapper.Map<CartEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    return _mapper.Map<CartDto>(entity);
                }
            }
            return new CartDto();
        }

        public async Task<bool> UpdateCart(CartDto dto)
        {
            //throw new NotImplementedException();
            if (dto != null)
            {
                var cart = await _repository.GetById(dto.Id);
                if (cart != null)
                {
                    dto.CartStatus = cart.CartStatus;
                    var entity = await _repository.Update(_mapper.Map<CartEntity>(dto));
                    var result = await _repository.SaveChangesAsync();
                    if (result > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> UpdateCartStatus(Guid id, CartStatusEnum status)
        {
            //throw new NotImplementedException();
            var cart = await _repository.GetById(id);
            if (cart != null)
            {
                cart.CartStatus = status;
                var entity = await _repository.Update(cart);
                var result = await _repository.SaveChangesAsync();
                if (result > 0)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
