﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Cart.Domain.Dtos
{
    public class CartDto
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public CartStatusEnum CartStatus { get; set; }
        public ProductStatusEnum ProductStatus { get; set; }

        [ForeignKey("CustomerId")]
        public virtual UserDto Customer { get; set; }
    }
}
