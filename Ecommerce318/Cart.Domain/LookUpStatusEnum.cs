﻿using System.Text.Json.Serialization;

namespace Cart.Domain
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum LookUpStatusEnum
    {
        Active,
        Inactive,
        Removed
    }
}
