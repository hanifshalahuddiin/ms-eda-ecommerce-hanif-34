﻿using Framework.Core.Events;
//using LookUp.Domain;
using Microsoft.EntityFrameworkCore;
using Cart.Domain.Entities;
using Cart.Domain.Repositories;
using Cart.Domain.Services;

namespace Cart.Domain.Projections
{
    public record AttributeCreated(
        Guid? Id,
        AttributeTypeEnum Type,
        string Unit,
        LookUpStatusEnum Status,
        DateTime Modified
    );

    public record AttributeUpdated(
        Guid? Id,
        AttributeTypeEnum Type,
        string Unit,
        DateTime Modified
    );

    public record AttributeStatusChanged(
        Guid? Id,
        LookUpStatusEnum Status,
        DateTime Modified
    );

    public class ProductProjection
    {
        private readonly CartDbContext _context;
        public ProductProjection()
        {

        }

        //public static bool Handle(EventEnvelope<AttributeCreated> eventEnvelope)
        //{
        //    var (id, type, unit, status, modified) = eventEnvelope.Data;
        //    using (var context = new CartDbContext(CartDbContext.OnConfigure()))
        //    {
        //        AttributeEntity entity = new AttributeEntity()
        //        {
        //            Id = (Guid)id,
        //            Type = type,
        //            Unit = unit,
        //            Status = status,
        //            Modified = modified
        //        };

        //        context.Attributes.Add(entity);
        //        context.SaveChanges();
        //    }

        //    return true; //new AttributeCreated(id, type, unit, status, modified);
        //}

        //public static bool Handle(EventEnvelope<AttributeUpdated> eventEnvelope)
        //{
        //    var (id, type, unit, modified) = eventEnvelope.Data;
        //    using (var context = new CartDbContext(CartDbContext.OnConfigure()))
        //    {
        //        AttributeEntity entity = context.Set<AttributeEntity>().Find(id);
        //        entity.Type = type;
        //        entity.Unit = unit;
        //        entity.Modified = modified;
        //        context.SaveChanges();
        //    }

        //    return true; //new AttributeCreated(id, type, unit, status, modified);
        //}

        //public static bool Handle(EventEnvelope<AttributeStatusChanged> eventEnvelope)
        //{
        //    var (id, status, modified) = eventEnvelope.Data;
        //    using (var context = new CartDbContext(CartDbContext.OnConfigure()))
        //    {
        //        AttributeEntity entity = context.Set<AttributeEntity>().Find(id);
        //        entity.Status = status;
        //        entity.Modified = modified;
        //        context.SaveChanges();
        //    }

        //    return true; //new AttributeCreated(id, type, unit, status, modified);
        //}
    }
}
