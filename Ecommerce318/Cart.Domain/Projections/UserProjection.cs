﻿using Framework.Core.Events;
//using LookUp.Domain;
using Microsoft.EntityFrameworkCore;
using Cart.Domain.Entities;
using Cart.Domain.Repositories;
using Cart.Domain.Services;

namespace Cart.Domain.Projections
{
    public record UserCreated(
        Guid? Id,
        string UserName,
        string Email,
        UserStatusEnum Status,
        UserTypeEnum Type,
        DateTime Modified
    );

    public record UserUpdated(
        Guid? Id,
        string UserName,
        string Email,
        UserTypeEnum Type,
        DateTime Modified
    );

    public record UserStatusChanged(
        Guid? Id,
        UserStatusEnum Status,
        DateTime Modified
    );

    public record UserDeleted(
        Guid? Id,
        UserStatusEnum Status,
        DateTime Modified
    );

    public class UserProjection
    {
        private readonly CartDbContext _context;
        public UserProjection()
        {

        }

        public static bool Handle(EventEnvelope<UserCreated> eventEnvelope)
        {
            var (id, userName, email, status, type, modified) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                UserEntity entity = new UserEntity()
                {
                    Id = (Guid)id,
                    UserName = userName,
                    Email = email,
                    Status = status,
                    Type = type,
                    Modified = modified
                };

                context.Users.Add(entity);
                context.SaveChanges();
            }

            return true; //new UserCreated(id, userName, email, status, type, modified);
        }

        public static bool Handle(EventEnvelope<UserUpdated> eventEnvelope)
        {
            var (id, userName, email, type, modified) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                UserEntity entity = context.Set<UserEntity>().Find(id);
                entity.UserName = userName;
                entity.Email = email;
                entity.Type = type;
                entity.Modified = modified;
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<UserStatusChanged> eventEnvelope)
        {
            var (id, status, modified) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                UserEntity entity = context.Set<UserEntity>().Find(id);
                entity.Status = status;
                entity.Modified = modified;
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<UserDeleted> eventEnvelope)
        {
            var (id, status, modified) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                UserEntity entity = context.Set<UserEntity>().Find(id);
                entity.Status = status;
                entity.Modified = modified;
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }
    }
}
