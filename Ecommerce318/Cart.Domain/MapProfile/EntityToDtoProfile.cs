﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Entities;

namespace Cart.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile() : base("Entity to Dto profile")
        {

            //CreateMap<UserEntity, UserDto>().ReverseMap();
            CreateMap<UserEntity, UserDto>();
            CreateMap<UserDto, UserEntity>();

            //CreateMap<UserEntity, UserDto>().ReverseMap();
            CreateMap<CartEntity, CartDto>();
            CreateMap<CartDto, CartEntity>();

            //CreateMap<ProductEntity, ProductDto>();
            //CreateMap<ProductDto, ProductEntity>();

            //CreateMap<CartProductsEntity, CartProductsDto>();
            //CreateMap<CartProductsDto, CartProductsEntity>();
        }
    }
}