﻿
using FluentValidation;
using FluentValidation.Results;
using LookUp.Domain;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

// [Route()], [ApiController], ControllerBase, [FromBody]
// IActionResult: { OK(), BadRequest(), NoContent() }
// [HttpGet], [HttpPost], [HttpPut]
using Microsoft.AspNetCore.Mvc;

namespace LookUp.OpenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {
        private readonly ICurrencyService _service;
        private readonly ILogger<CurrencyController> _logger;
        private IValidator<CurrencyDto> _validator;

        public CurrencyController(ICurrencyService service, ILogger<CurrencyController> logger, IValidator<CurrencyDto> validator)
        {
            _service = service;
            _logger = logger;
            _validator = validator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _service.All());
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CurrencyDto payload, CancellationToken ct)
        {
            try
            {
                ValidationResult result = await _validator.ValidateAsync(payload);
                if (!result.IsValid)
                {
                    return BadRequest(result);
                }

                var dto = await _service.AddCurrency(payload);
                if (dto != null)
                {
                    return Ok(dto);
                }
            }
            catch (OperationCanceledException ex) when (ct.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
                throw;
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Put(Guid id, [FromBody] CurrencyDto payload, CancellationToken ct)
        {
            try
            {
                if (payload != null)
                {
                    payload.Id = id;
                    var isUpdated = await _service.UpdateCurrency(payload);
                    if (isUpdated)
                    {
                        return Ok(isUpdated);
                    }
                }
            }
            catch (OperationCanceledException ex) when (ct.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id, CancellationToken ct)
        {
            try
            {
                var isDeleted = await _service.DeleteCurrency(id);
                if (isDeleted)
                    return Ok(isDeleted);
            }
            catch (OperationCanceledException ex) when (ct.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpPut("status")]
        public async Task<IActionResult> Put(Guid id, LookUpStatusEnum status, CancellationToken ct)
        {
            try
            {
                var isUpdated = await _service.UpdateCurrencyStatus(id, status);
                if (isUpdated)
                {
                    return Ok(isUpdated);
                }
            }
            catch (OperationCanceledException ex) when (ct.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }



        [HttpGet("by id")]
        public async Task<IActionResult> SelectAllById(Guid id, CancellationToken ct)
        {
            try
            {
                var getById = await _service.GetCurrencyById(id);
                return Ok(getById);
            }
            catch (OperationCanceledException ex) when (ct.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpPut("Manual")]
        public async Task<IActionResult> UpdateManual(Guid id, [FromBody] CurrencyDtoManual manual, CancellationToken ct)
        {
            try
            {
                var isUpdated = await _service.UpdateCurrencyManual(id, manual);
                if (isUpdated)
                    return Ok(isUpdated);
            }
            catch (OperationCanceledException ex) when (ct.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpPut("Enum")]
        public async Task<IActionResult> UpdateEnum(Guid id, CurrencyNameEnum name, CurrencyCodeEnum code, CurrencySymbolEnum symbol, CancellationToken ct)
        {
            try
            {
                var isUpdated = await _service.UpdateCurrencyEnum(id, name, code, symbol);
                if (isUpdated)
                    return Ok(isUpdated);
            }
            catch (OperationCanceledException ex) when (ct.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }
    }
}
