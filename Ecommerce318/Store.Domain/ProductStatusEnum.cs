﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Store.Domain
{
    //internal class ProductStatusEnum
    //{
    //}

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum ProductStatusEnum
    {
        Active,
        Inactive,
        Removed
    }
}