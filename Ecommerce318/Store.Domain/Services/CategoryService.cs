﻿using AutoMapper;
using Store.Domain.EventEnvelopes.Category;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using Store.Domain;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.Repositories;

namespace Store.Domain.Services
{
    public interface ICategoryService
    {
        Task<IEnumerable<CategoryDto>> All();
        Task<CategoryDto> GetCategoryById(Guid id);
        Task<CategoryDto> AddCategory(CategoryDto dto);
        Task<bool> UpdateCategory(CategoryDto dto);
        Task<bool> UpdateCategoryStatus(Guid id, ProductStatusEnum status);
        Task<bool> DeleteCategory(Guid id);

        Task<bool> UpdateCategoryManual(Guid id, CategoryDtoManual manual);
        //Task<bool> UpdateCategoryEnum(Guid id, CategoryTypeEnum type);
    }

    public class CategoryService : ICategoryService
    {
        private ICategoryRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public CategoryService(ICategoryRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<IEnumerable<CategoryDto>> All()
        {
            return _mapper.Map<IEnumerable<CategoryDto>>(await _repository.GetAll());
        }

        public async Task<CategoryDto> GetCategoryById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<CategoryDto>(result);
                }
            }
            return new CategoryDto();
            //return null;
        }

        public async Task<CategoryDto> AddCategory(CategoryDto dto)
        {
            if (dto != null)
            {
                dto.Status = ProductStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<CategoryEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {

                    //Record Kafka
                    var externalEvent = new EventEnvelope<CategoryCreated>(
                        CategoryCreated.Create(
                            entity.Id,
                            entity.Name,
                            entity.Description,
                            entity.Status,
                            entity.Modified
                        )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                    return _mapper.Map<CategoryDto>(entity);
                }
            }
            return new CategoryDto();
        }

        public async Task<bool> UpdateCategory(CategoryDto dto)
        {
            if (dto != null)
            {
                var category = await _repository.GetById(dto.Id);
                if (category != null)
                {
                    dto.Status = category.Status;
                    var entity = await _repository.Update(_mapper.Map<CategoryEntity>(dto));
                    var result = await _repository.SaveChangesAsync();

                    //Record Kafka
                    var externalEvent = new EventEnvelope<CategoryUpdated>(
                        CategoryUpdated.Update(
                            entity.Id,
                            entity.Name,
                            entity.Description,
                            entity.Status,
                            entity.Modified
                        )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                    if (result > 0) return true;
                }
            }
            return false;
        }

        public async Task<bool> UpdateCategoryStatus(Guid id, ProductStatusEnum status)
        {
            var category = await _repository.GetById(id);
            if (category != null)
            {
                category.Status = status;
                var entity = await _repository.Update(category);
                var result = await _repository.SaveChangesAsync();

                //Record Kafka
                var externalEvent = new EventEnvelope<CategoryStatusUpdated>(
                    CategoryStatusUpdated.UpdateStatus(
                        entity.Id,
                        entity.Name,
                        entity.Description,
                        entity.Status,
                        entity.Modified
                    )
                );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                if (result > 0) return true;
            }
            return false;
        }

        //Delete
        public async Task<bool> DeleteCategory(Guid id)
        {
            var category = await _repository.GetById(id);
            if (category != null)
            {
                category.Status = ProductStatusEnum.Removed;
                var entity = await _repository.Delete(category);
                var result = await _repository.SaveChangesAsync();

                //Record Kafka
                var externalEvent = new EventEnvelope<CategoryDeleted>(
                    CategoryDeleted.Delete(
                        entity.Id,
                        entity.Name,
                        entity.Description,
                        entity.Status,
                        entity.Modified
                    )
                );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                if (result > 0) return true;
            }
            return false;
        }



        //update data manual

        public async Task<bool> UpdateCategoryManual(Guid id, CategoryDtoManual manual)
        {
            var category = await _repository.GetById(id);
            if (category != null)
            {
                category.Name = manual.Name;
                category.Description = manual.Description;
                var entity = await _repository.Update(_mapper.Map<CategoryEntity>(category));
                var result = await _repository.SaveChangesAsync();

                //Record Kafka
                var externalEvent = new EventEnvelope<CategoryUpdated>(
                    CategoryUpdated.Update(
                        entity.Id,
                        entity.Name,
                        entity.Description,
                        entity.Status,
                        entity.Modified
                    )
                );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                if (result > 0)
                {
                    return true;
                }
            }
            return false;
        }

        ////Update Data Enum
        //public async Task<bool> UpdateUserEnum(Guid id, CategoryTypeEnum type)
        //{
        //    var user = await _repository.GetById(id);
        //    if (user != null)
        //    {
        //        user.Type = type;
        //        var entity = await _repository.Update(user);
        //        var result = await _repository.SaveChangesAsync();

        //        //Record Kafka
        //        var externalEvent = new EventEnvelope<CategoryDeleted>(
        //            CategoryDeleted.Delete(
        //                entity.Id,
        //                entity.Name,
        //                entity.Description,
        //                entity.Status,
        //                entity.Modified
        //            )
        //        );
        //        await _externalEventProducer.Publish(externalEvent, new CancellationToken());

        //        if (result > 0) return true;
        //    }

        //    return false;
        //}
    }
}
