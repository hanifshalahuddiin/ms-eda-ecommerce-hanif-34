﻿using AutoMapper;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using Store.Domain;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.EventEnvelopes.Product;
using Store.Domain.Repositories;

namespace Store.Domain.Services
{
    public interface IGalleryService
    {
        Task<IEnumerable<GalleryDto>> All();
        Task<GalleryDto> GetGalleryById(Guid id);
        Task<GalleryDto> AddGallery(GalleryDto dto);
        //Task<bool> UpdateGallery(GalleryDto dto);
        //Task<bool> UpdateGalleryStatus(Guid id, ProductStatusEnum status);
        //Task<bool> DeleteGallery(Guid id);


        //Task<bool> UpdateGalleryManual(Guid id, ProductDtoManual manual);
        //Task<bool> UpdateProductEnum(Guid id, LookUpStatusEnum status, AttributeTypeEnum type);
    }

    public class GalleryService : IGalleryService
    {
        private IGalleryRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public GalleryService(IGalleryRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<IEnumerable<GalleryDto>> All()
        {
            return _mapper.Map<IEnumerable<GalleryDto>>(await _repository.GetAll());
        }

        public async Task<GalleryDto> GetGalleryById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<GalleryDto>(result);
                }
            }
            return new GalleryDto();
        }

        public async Task<GalleryDto> AddGallery(GalleryDto dto)
        {
            if (dto != null)
            {
                dto.Status = ProductStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<GalleryEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {

                    ////Record Kafka
                    //var externalEvent = new EventEnvelope<ProductCreated>(
                    //    ProductCreated.Create(
                    //        entity.Id,
                    //        entity.CategoryId,
                    //        entity.AttributeId,
                    //        entity.SKU,
                    //        entity.Name,
                    //        entity.Description,
                    //        entity.Price,
                    //        entity.Volume,
                    //        entity.Sold,
                    //        entity.Stock,
                    //        entity.Status,
                    //        entity.Modified
                    //    )
                    //);
                    //await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                    return _mapper.Map<GalleryDto>(entity);
                }
            }
            return new GalleryDto();
        }

        //public async Task<bool> UpdateGallery(GalleryDto dto)
        //{
        //    if (dto != null)
        //    {
        //        var product = await _repository.GetById(dto.Id);
        //        if (product != null)
        //        {
        //            dto.Status = product.Status;
        //            var entity = await _repository.Update(_mapper.Map<GalleryEntity>(dto));
        //            var result = await _repository.SaveChangesAsync();

        //            //Record Kafka
        //            var externalEvent = new EventEnvelope<GalleryUpdated>(
        //                GalleryUpdated.Update(
        //                    entity.Id,
        //                    entity.FileLink,
        //                    entity.Name,
        //                    entity.Description,
        //                    entity.Status,
        //                    entity.Modified
        //                )
        //            );
        //            await _externalEventProducer.Publish(externalEvent, new CancellationToken());

        //            if (result > 0) return true;
        //        }
        //    }
        //    return false;
        //}

        //public async Task<bool> GalleryroductStatus(Guid id, ProductStatusEnum status)
        //{
        //    var product = await _repository.GetById(id);
        //    if (product != null)
        //    {
        //        product.Status = status;
        //        var entity = await _repository.Update(product);
        //        var result = await _repository.SaveChangesAsync();

        //        //Record Kafka
        //        var externalEvent = new EventEnvelope<ProductStatusUpdated>(
        //            ProductStatusUpdated.UpdateStatus(
        //                entity.Id,
        //                entity.CategoryId,
        //                entity.AttributeId,
        //                entity.SKU,
        //                entity.Name,
        //                entity.Description,
        //                entity.Price,
        //                entity.Volume,
        //                entity.Sold,
        //                entity.Stock,
        //                entity.Status,
        //                entity.Modified
        //            )
        //        );
        //        await _externalEventProducer.Publish(externalEvent, new CancellationToken());

        //        if (result > 0) return true;
        //    }
        //    return false;
        //}

        ////Delete
        //public async Task<bool> DeleteGallery(Guid id)
        //{
        //    var product = await _repository.GetById(id);
        //    if (product != null)
        //    {
        //        product.Status = ProductStatusEnum.Removed;
        //        var entity = await _repository.Delete(product);
        //        var result = await _repository.SaveChangesAsync();

        //        //Record Kafka
        //        var externalEvent = new EventEnvelope<ProductDeleted>(
        //            ProductDeleted.Delete(
        //                entity.Id,
        //                entity.CategoryId,
        //                entity.AttributeId,
        //                entity.SKU,
        //                entity.Name,
        //                entity.Description,
        //                entity.Price,
        //                entity.Volume,
        //                entity.Sold,
        //                entity.Stock,
        //                entity.Status,
        //                entity.Modified
        //            )
        //        );
        //        await _externalEventProducer.Publish(externalEvent, new CancellationToken());

        //        if (result > 0) return true;
        //    }
        //    return false;
        //}



        //update data manual

        //public async Task<bool> UpdateGalleryManual(Guid id, GalleryDtoManual manual)
        //{
        //    var product = await _repository.GetById(id);
        //    if (product != null)
        //    {
        //        product.Name = manual.Name;
        //        product.Description = manual.Description;
        //        var entity = await _repository.Update(_mapper.Map<GalleryEntity>(product));
        //        var result = await _repository.SaveChangesAsync();

        //        //Record Kafka
        //        var externalEvent = new EventEnvelope<ProductUpdated>(
        //            ProductUpdated.Update(
        //                entity.Id,
        //                entity.CategoryId,
        //                entity.AttributeId,
        //                entity.SKU,
        //                entity.Name,
        //                entity.Description,
        //                entity.Price,
        //                entity.Volume,
        //                entity.Sold,
        //                entity.Stock,
        //                entity.Status,
        //                entity.Modified
        //            )
        //        );
        //        await _externalEventProducer.Publish(externalEvent, new CancellationToken());

        //        if (result > 0)
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        ////Update Data Enum
        //public async Task<bool> UpdateUserEnum(Guid id, CategoryTypeEnum type)
        //{
        //    var user = await _repository.GetById(id);
        //    if (user != null)
        //    {
        //        user.Type = type;
        //        var entity = await _repository.Update(user);
        //        var result = await _repository.SaveChangesAsync();

                ////Record Kafka
                //var externalEvent = new EventEnvelope<ProductUpdated>(
                //    ProductUpdated.Update(
                //        entity.Id,
                //        entity.CategoryId,
                //        entity.AttributeId,
                //        entity.SKU,
                //        entity.Name,
                //        entity.Description,
                //        entity.Price,
                //        entity.Volume,
                //        entity.Sold,
                //        entity.Stock,
                //        entity.Status,
                //        entity.Modified
                //    )
                //);
                //await _externalEventProducer.Publish(externalEvent, new CancellationToken());

        //        if (result > 0) return true;
        //    }

        //    return false;
        //}
    }
}
