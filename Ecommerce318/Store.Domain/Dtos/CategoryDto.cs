﻿
namespace Store.Domain.Dtos
{
    public class CategoryDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ProductStatusEnum Status { get; internal set; }
    }

    public class CategoryDtoManual
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
