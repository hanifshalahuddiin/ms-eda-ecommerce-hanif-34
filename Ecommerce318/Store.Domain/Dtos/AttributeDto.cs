﻿
namespace Store.Domain.Dtos
{
    public class AttributeDto
    {
        public Guid Id { get; set; }
        public AttributeTypeEnum Type { get; set; }
        public string Unit { get; set; }
        //public ProductStatusEnum Status { get; internal set; }
        public LookUpStatusEnum Status { get; internal set; }
    }
}
