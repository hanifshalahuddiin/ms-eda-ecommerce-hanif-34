﻿using System.Text.Json.Serialization;

namespace Store.Domain
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum LookUpStatusEnum
    {
        Active,
        Inactive,
        Removed
    }
}
