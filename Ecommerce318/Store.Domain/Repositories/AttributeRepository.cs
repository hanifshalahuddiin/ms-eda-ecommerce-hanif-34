﻿using Microsoft.EntityFrameworkCore;
using Store.Domain.Entities;

namespace Store.Domain.Repositories
{
    public interface IAttributeRepository
    {
        Task<int> GetCount();
        Task<IEnumerable<AttributeEntity>> GetAll();
        //Task<IEnumerable<CategoryEntity>> GetAll();
        Task<IEnumerable<AttributeEntity>> GetPaged(int page, int size);
        Task<AttributeEntity> GetById(Guid id);
        Task<AttributeEntity> Add(AttributeEntity entity);
        Task<AttributeEntity> Update(AttributeEntity entity);
        void Delete(AttributeEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);


        //Task<IEnumerable<AttributeEntity>> GetActive(LookUpStatusEnum status);
    }

    public class AttributeRepository : IAttributeRepository
    {
        protected readonly StoreDbContext _context;

        public AttributeRepository(StoreDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<IEnumerable<AttributeEntity>> GetAll()
        {
            //throw new NotImplementedException();
            var result = await _context.Set<AttributeEntity>().ToListAsync();
            return result;
        }

        //public async Task<IEnumerable<CategoryEntity>> GetAll()
        //{
        //    //throw new NotImplementedException();
        //    var result = await _context.Set<CategoryEntity>().ToListAsync();
        //    return result;
        //}

        public async Task<AttributeEntity> GetById(Guid id)
        {
            //throw new NotImplementedException();
            var result = await _context.Set<AttributeEntity>().FindAsync(id);
            return result;
        }

        public async Task<AttributeEntity> Add(AttributeEntity entity)
        {
            //throw new NotImplementedException();
            _context.Set<AttributeEntity>().Add(entity);
            return entity;
        }

        public async Task<AttributeEntity> Update(AttributeEntity entity)
        {
            //throw new NotImplementedException();
            _context.Set<AttributeEntity>().Update(entity);
            return entity;
        }

        public void Delete(AttributeEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            //throw new NotImplementedException();
            var result = await _context.SaveChangesAsync(cancellationToken);
            return result;
        }

        public Task<int> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<AttributeEntity>> GetPaged(int page, int size)
        {
            throw new NotImplementedException();
        }




        //public async Task<IEnumerable<AttributeEntity>> GetActive(LookUpStatusEnum status)
        //{
        //    var result = await _context.Set<AttributeEntity>().Find(id);
        //    //return result;

        //    var result = await _context.Set<AttributeEntity>().ToListAsync();
        //    return result;
        //}
    }
}
