﻿using Microsoft.EntityFrameworkCore;
using Store.Domain.Entities;

namespace Store.Domain.Repositories
{
    public interface IGalleryRepository
    {
        // Task<int> GetCount();
        Task<IEnumerable<GalleryEntity>> GetAll();
        //Task<IEnumerable<ProductEntity>> GetPaged(int page, int size);
        Task<GalleryEntity> GetById(Guid id);
        Task<GalleryEntity> Add(GalleryEntity entity);
        Task<GalleryEntity> Update(GalleryEntity entity);
        Task<GalleryEntity> Delete(GalleryEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }

    public class GalleryRepository : IGalleryRepository
    {
        protected readonly StoreDbContext _context;

        public GalleryRepository(StoreDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<IEnumerable<GalleryEntity>> GetAll()
        {
            return await _context.Set<GalleryEntity>().ToListAsync();
        }

        public async Task<GalleryEntity> GetById(Guid id)
        {
            return await _context.Set<GalleryEntity>().FindAsync(id);
        }

        public async Task<GalleryEntity> Add(GalleryEntity entity)
        {
            /*await*/ _context.Set<GalleryEntity>().Add/*Async*/(entity);
            return entity;
        }

        public async Task<GalleryEntity> Update(GalleryEntity entity)
        {
            _context.Set<GalleryEntity>().Update(entity);
            return entity;
        }

        public async Task<GalleryEntity> Delete(GalleryEntity entity)
        {
            _context.Set<GalleryEntity>().Update(entity);
            return entity;
        }

        //public Task<int> GetCount()
        //{
        //    throw new NotImplementedException();
        //}

        //public Task<IEnumerable<CategoryEntity>> GetPaged(int page, int size)
        //{
        //    throw new NotImplementedException();
        //}

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }

}
