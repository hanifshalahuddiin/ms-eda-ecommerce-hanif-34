﻿using Store.Domain;

namespace Store.Domain.EventEnvelopes.Category
{ 
    public record CategoryCreated(
        Guid Id,
        string Name,
        string Description,
        ProductStatusEnum Status,
        DateTime Modified
    )
    {
        public static CategoryCreated Create(
            Guid Id,
            string Name,
            string Description,
            ProductStatusEnum Status,
            DateTime Modified
        ) => new(Id, Name, Description, Status, Modified);
    }
}