﻿using Store.Domain;

namespace Store.Domain.EventEnvelopes.Category
{
    public record CategoryDeleted(
        Guid Id,
        string Name,
        string Description,
        ProductStatusEnum Status,
        DateTime Modified
    )
    {
        public static CategoryDeleted Delete(
            Guid Id,
            string Name,
            string Description,
            ProductStatusEnum Status,
            DateTime Modified
        ) => new(Id, Name, Description, Status, Modified);
    }
}