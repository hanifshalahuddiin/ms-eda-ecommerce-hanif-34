﻿using Store.Domain;

namespace Store.Domain.EventEnvelopes.Category
{
    public record CategoryUpdated(
        Guid Id,
        string Name,
        string Description,
        ProductStatusEnum Status,
        DateTime Modified
    )
    {
        public static CategoryUpdated Update(
            Guid Id,
            string Name,
            string Description,
            ProductStatusEnum Status,
            DateTime Modified
        ) => new(Id, Name, Description, Status, Modified);
    }
}