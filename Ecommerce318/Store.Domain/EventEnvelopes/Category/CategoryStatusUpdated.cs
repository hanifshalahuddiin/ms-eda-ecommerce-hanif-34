﻿using Store.Domain;

namespace Store.Domain.EventEnvelopes.Category
{
    public record CategoryStatusUpdated(
        Guid Id,
        string Name,
        string Description,
        ProductStatusEnum Status,
        DateTime Modified
    )
    {
        public static CategoryStatusUpdated UpdateStatus(
            Guid Id,
            string Name,
            string Description,
            ProductStatusEnum Status,
            DateTime Modified
        ) => new(Id, Name, Description, Status, Modified);
    }
}