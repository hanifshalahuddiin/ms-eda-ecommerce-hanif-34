﻿using Store.Domain;

namespace Store.Domain.EventEnvelopes.Product
{
    public record ProductStatusUpdated(
        Guid Id,
        Guid CategoryId,
        Guid AttributeId,
        string SKU,
        string Name,
        string Description,
        decimal Price,
        decimal Volume,
        int Sold,
        int Stock,
        ProductStatusEnum Status,
        DateTime Modified
    )
    {
        public static ProductStatusUpdated UpdateStatus(
        Guid Id,
        Guid CategoryId,
        Guid AttributeId,
        string SKU,
        string Name,
        string Description,
        decimal Price,
        decimal Volume,
        int Sold,
        int Stock,
        ProductStatusEnum Status,
        DateTime Modified
        ) => new(Id, CategoryId, AttributeId, SKU, Name, Description, Price, Volume, Sold, Stock, Status, Modified);
    }
}