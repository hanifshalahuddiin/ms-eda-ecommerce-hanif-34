﻿using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;

namespace Store.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile() : base("Entity to Dto profile")
        {
            //CreateMap<AttributeEntity, AttributeDto>().ReverseMap();
            CreateMap<AttributeEntity, AttributeDto>();
            CreateMap<AttributeDto, AttributeEntity>();

            //CreateMap<CategoryEntity, CategoryDto>().ReverseMap();
            CreateMap<CategoryEntity, CategoryDto>();
            CreateMap<CategoryDto, CategoryEntity>();

            //CreateMap<ProductEntity, ProductDto>().ReverseMap();
            CreateMap<ProductEntity, ProductDto>();
            CreateMap<ProductDto, ProductEntity>();

            //CreateMap<GalleryEntity, GalleryDto>().ReverseMap();
            //CreateMap<GalleryEntity, GalleryDto>();
            //CreateMap<GalleryDto, GalleryEntity>();
        }
    }
}