﻿using User.Domain;
using User.Domain.Dtos;
using User.Domain.Services;

namespace User.GraphQL.Schemas.Mutations
{
    [ExtendObjectType(Name = "Mutation")]
    public class UserMutation
    {
        private readonly IUserService _service;

        public UserMutation(IUserService service)
        {
            _service = service;
        }

        public async Task<UserDto> AddUserAsync(UserTypeInput user)
        {
            UserDto dto = new UserDto();
            dto.UserName = user.UserName;
            dto.Password = user.Password;
            dto.FirstName = user.FirstName;
            dto.LastName = user.LastName;
            dto.Email = user.Email;
            dto.Type = user.Type;
            var result = await _service.AddUser(dto);
            return result;
        }

        public async Task<UserDto> EditUserAsync(Guid id, UserTypeInput user)
        {
            UserDto dto = new UserDto();
            dto.Id = id;
            dto.UserName = user.UserName;
            dto.Password = user.Password;
            dto.FirstName = user.FirstName;
            dto.LastName = user.LastName;
            dto.Email = user.Email;
            var result = await _service.UpdateUser(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("User not found", "404"));
            }
            return dto;
        }

        public async Task<UserDto> ChangeUserStatusAsync(Guid id, UserStatusEnum status)
        {
            //AttributeDto dto = new AttributeDto();
            //dto.Id = id;
            //dto.Status = status;

            var result = await _service.UpdateUserStatus(id, status);
            if (result)
            {
                return await _service.GetUserById(id);
            }
            throw new GraphQLException(new Error("User not found", "404"));

            //return dto;
        }

        public async Task<UserDto> RemoveUserAsync(Guid id, UserStatusEnum status)
        {
            throw new GraphQLException(new Error("User not found", "404"));
            //var result
        }
    }
}
