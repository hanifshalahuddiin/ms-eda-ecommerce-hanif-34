﻿using HotChocolate.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using User.Domain.Dtos;
using User.Domain.Services;

namespace User.GraphQL.Schemas.Queries
{
    [ExtendObjectType("Query")]
    public class AuthQuery
    {
        private readonly IUserService _service;
        private readonly IConfiguration _configuration;

        public AuthQuery(IUserService service, IConfiguration configuration)
        {
            _service = service;
            _configuration = configuration;
        }

        public string IndexAll()
        {
            return "This is for all";
        }

        [Authorize]

        public string IndexAuthorized()
        {
            return "This is for authorized only";
        }

        [Authorize(Roles = new[] {"customer"})]
        public string IndexCustomer()
        {
            return "This is for customer";
        }

        [Authorize(Roles = new[] { "admin" })]
        public string IndexAdmin()
        {
            return "This is for admin";
        }

        public async Task<LoginDto> LoginAsync(string userName, string password)
        {
            LoginDto result = await _service.Login(userName, password);
            if (result != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, result.UserName),
                    new Claim("FullName", result.FullName)
                };

                foreach (var item in result.Roles)
                {
                    claims.Add(
                        new Claim(ClaimTypes.Role, item.ToLower())
                        );
                }

                var token = GetToken(claims);
                result.Token = new JwtSecurityTokenHandler().WriteToken(token);
                result.Expiration = token.ValidTo;
            }

            return result;
        }

        private JwtSecurityToken GetToken(List<Claim> authClaims)
        {

            //"JWT": {
            //    "ValidAudience": true,
            //    "ValidIssuer": true,
            //    "ExpireDays": 60,
            //    "Secret": "JWAuthorization09123!@$9832"
            // },
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

            var jwt = new JwtSecurityToken(
                issuer: _configuration["JWT:ValidIssuer"],
                audience: _configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddDays(Convert.ToDouble(_configuration["JWT:ExpireDays"])),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );

            return jwt;
        }
     }
}
