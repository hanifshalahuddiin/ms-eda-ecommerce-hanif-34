﻿using User.Domain.Dtos;
using User.Domain.Services;

namespace User.GraphQL.Schemas.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class UserQuery
    {
        private readonly IUserService _service;

        public UserQuery(IUserService service)
        {
            _service = service;
        }

        public async Task<IEnumerable<UserDto>> GetAllUserAsync()
        {
            IEnumerable<UserDto> result = await _service.All();
            return result;
        }

        public async Task<UserDto> GetUserByIdAsync(Guid id)
        {
            return await _service.GetUserById(id);
        }
    }
}
