﻿using Cart.Domain;

namespace Cart.GraphQL.Schemas.Mutations
{
    public class CartTypeInput
    {
        public CartStatusEnum Status { get; set; }
    }
}


//public Guid Id { get; set; }
//public Guid CustomerId { get; set; }
//public CartStatusEnum CartStatus { get; set; } = default!;
//public ProductStatusEnum ProductStatus { get; set; } = default!;
//public DateTime Modified { get; internal set; } = DateTime.Now;