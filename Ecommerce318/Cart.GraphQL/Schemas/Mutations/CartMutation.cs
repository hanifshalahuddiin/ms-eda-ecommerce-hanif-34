﻿using Cart.Domain;
using Cart.Domain.Dtos;
using Cart.Domain.Services;

namespace Cart.GraphQL.Schemas.Mutations
{
    [ExtendObjectType(Name = "Mutation")]
    public class CartMutation
    {
        private readonly ICartService _service;

        public CartMutation(ICartService service)
        {
            _service = service;
        }
        public async Task<CartDto> AddCartAsync(CartTypeInput cart)
        {
            CartDto dto = new CartDto();
            dto.CartStatus = cart.Status;
            var result = await _service.AddCart(dto);
            return result;
        }

        public async Task<CartDto> EditCartAsync(Guid id, CartTypeInput cart)
        {
            CartDto dto = new CartDto();
            dto.Id = id;
            dto.CartStatus = cart.Status;
            var result = await _service.UpdateCart(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Cart not found", "404"));
            }
            return dto;
        }

        public async Task<CartDto> ChangeCartStatusAsync(Guid id, CartStatusEnum status)
        {
            //AttributeDto dto = new AttributeDto();
            //dto.Id = id;
            //dto.Status = status;

            var result = await _service.UpdateCartStatus(id, status);
            if (result)
            {
                return await _service.GetCartById(id);
            }
            throw new GraphQLException(new Error("Cart not found", "404"));

            //return dto;
        }

        public async Task<CartDto> RemoveCartAsync(Guid id, LookUpStatusEnum status)
        {
            throw new GraphQLException(new Error("Cart not found", "404"));
            //var result
        }
    }
}


//public class CartEntity
//{
//    public Guid Id { get; set; }
//    public Guid CustomerId { get; set; }
//    public CartStatusEnum CartStatus { get; set; } = default!;
//    public ProductStatusEnum ProductStatus { get; set; } = default!;
//    public DateTime Modified { get; internal set; } = DateTime.Now;

//    [ForeignKey("CustomerId")]
//    public virtual UserEntity Customer { get; set; }
//}
