using Framework.Kafka;
using Cart.Domain;
using Cart.Domain.MapProfile;
using Cart.Domain.Repositories;
using Cart.Domain.Services;
using Cart.GraphQL.Schemas.Mutations;
using Cart.GraphQL.Schemas.Queries;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("LookUp_Db_Conn").Value);

    options
        .EnableSensitiveDataLogging(false)
        .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});

builder.Services.AddKafkaProducer();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services
    .AddScoped<Query>()
    //.AddScoped<UserQuery>()
    .AddScoped<CartQuery>()
    //.AddScoped<ProductQuery>()
    //.AddScoped<CartProductsQuery>()
    .AddScoped<Mutation>()
    //.AddScoped<UserMutation>()
    .AddScoped<CartMutation>()
    //.AddScoped<ProductMutation>()
    //.AddScoped<CartProductsMutation>()
    .AddScoped<IUserRepository, UserRepository>()
    .AddScoped<IUserService, UserService>()
    .AddScoped<ICartRepository, CartRepository>()
    .AddScoped<ICartService, CartService>()
    //.AddScoped<IProductRepository, ProductRepository>()
    //.AddScoped<IProductService, ProductService>()
    //.AddScoped<ICartProductsRepository, CartProductsRepository>()
    //.AddScoped<ICartProductsService, CartProductsService>()
    .AddGraphQLServer()
    .AddQueryType<Query>()
    //.AddTypeExtension<UserQuery>()
    .AddTypeExtension<CartQuery>()
    //.AddTypeExtension<ProductQuery>()
    //.AddTypeExtension<CartProductsQuery>()
    .AddMutationType<Mutation>()
    //.AddTypeExtension<UserMutation>()
    .AddTypeExtension<CartMutation>();
    //.AddTypeExtension<ProductMutation>();
    //.AddTypeExtension<CartProductsMutation>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapGraphQL();

app.Run();
