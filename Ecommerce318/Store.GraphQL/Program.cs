using Framework.Kafka;
using LookUp.GraphQL.Schemas.Mutations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using Store.Domain;
using Store.Domain.MapProfile;
using Store.Domain.Repositories;
using Store.Domain.Services;
using Store.GraphQL.Schemas.Mutations;
using Store.GraphQL.Schemas.Queries;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("Store_Db_Conn").Value);

    options
        .EnableSensitiveDataLogging(false)
        .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});

builder.Services.AddKafkaProducer();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services
    .AddScoped<Query>()
    .AddScoped<CategoryQuery>()
    //.AddScoped<GalleryQuery>()
    .AddScoped<Mutation>()
    //.AddScoped<CategoryMutation>()
    .AddScoped<GalleryMutation>()
    .AddScoped<ICategoryRepository, CategoryRepository>()
    .AddScoped<ICategoryService, CategoryService>()
    .AddScoped<IGalleryRepository, GalleryRepository>()
    .AddScoped<IGalleryService, GalleryService>()
    .AddGraphQLServer()
    .AddType<UploadType>()
    .AddQueryType<Query>()
    .AddTypeExtension<CategoryQuery>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapGraphQL();

app.UseFileServer(new FileServerOptions()
{
    FileProvider = new PhysicalFileProvider(
        System.IO.Path.Combine(Directory.GetCurrentDirectory(), @"Resources/Images")
        ),
    RequestPath = new PathString("/img"),
    EnableDirectoryBrowsing = true
});

app.Run();
