﻿using LookUp.GraphQL.Schemas.Mutations;
using Store.Domain;
using Store.Domain.Dtos;
using Store.Domain.Services;
using Path = System.IO.Path;

namespace Store.GraphQL.Schemas.Mutations
{
    [ExtendObjectType(typeof(Mutation))]
    public class GalleryMutation
    {
        private readonly IGalleryService _service;
        private string[] acceptedExt = new string[] { ".jpg", ".jpeg", ".png", ".gif" };

        public GalleryMutation(IGalleryService service/*, string[] acceptedExt*/)
        {
            _service = service;
            //this.acceptedExt = acceptedExt;
        }

        private string GetUniqueName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
                + "_" + Guid.NewGuid().ToString().Substring(0, 4)
                + Path.GetExtension(fileName);
        }



        public async Task<GalleryDto> AddGalleryAsync(GalleryTypeInput gallery)
        {
            try
            {
                GalleryDto dto = new GalleryDto();
                dto.Name = gallery.Name;
                dto.Description = gallery.Description;

                string fileExt = Path.GetExtension(gallery.File.Name);

                if (Array.IndexOf(acceptedExt, fileExt) != -1)
                {
                    var uniqueFileName = GetUniqueName(gallery.File.Name);
                    var uploads = Path.Combine("Resources", "Images");
                    var filePath = Path.Combine(uploads, uniqueFileName);

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await gallery.File.CopyToAsync(fileStream);
                    }

                    dto.FileLink = uniqueFileName;
                }

                return await _service.AddGallery(dto);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public async Task<CategoryDto> EditCategoryAsync(Guid id, CategoryTypeInput category)
        //{
        //    CategoryDto dto = new CategoryDto();
        //    dto.Id = id;
        //    dto.Unit = category.Unit;
        //    dto.Type = category.Type;
        //    var result = await _service.UpdateCategory(dto);
        //    if (!result)
        //    {
        //        throw new GraphQLException(new Error("Category not found", "404"));
        //    }
        //    return dto;
        //}

        //public async Task<CategoryDto> ChangeCategoryStatusAsync(Guid id, LookUpStatusEnum status)
        //{
        //    //AttributeDto dto = new AttributeDto();
        //    //dto.Id = id;
        //    //dto.Status = status;

        //    var result = await _service.UpdateCategoryStatus(id, status);
        //    if (result)
        //    {
        //        return await _service.GetCategoryById(id);
        //    }
        //    throw new GraphQLException(new Error("Category not found", "404"));

        //    //return dto;
        //}

        //public async Task<CategoryDto> RemoveCategoryAsync(Guid id, LookUpStatusEnum status)
        //{
        //    throw new GraphQLException(new Error("Category not found", "404"));
        //    //var result
        //}
    }
}


//public class CategoryEntity
//{
//    public Guid Id { get; set; }
//    public string Name { get; set; } = default!;
//    public string Description { get; set; } = default!;
//    public ProductStatusEnum Status { get; set; } = default!;
//    public DateTime Modified { get; internal set; } = DateTime.Now;
//}