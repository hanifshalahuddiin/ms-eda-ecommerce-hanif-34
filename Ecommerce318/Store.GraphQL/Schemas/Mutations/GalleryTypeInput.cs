﻿using Store.Domain;

namespace Store.GraphQL.Schemas.Mutations
{
    public class GalleryTypeInput
    {
        public string Name { get; set; } = default!;
        public string Description { get; set; } = default!;
        public ProductStatusEnum Status { get; set; } = default!;

        [GraphQLType(typeof(GalleryTypeInput))]
        public IFile File { get; set; } = default!;
    }
}