﻿using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.GraphQL.Schemas.Queries
{
    [ExtendObjectType(Name = "Query")]//annotation

    public class ProductQuery
    {
        private readonly IProductService _service;

        public ProductQuery(IProductService service)
        {
            _service = service;
        }

        public async Task<IEnumerable<ProductDto>> GetAllProductAsync()
        {
            IEnumerable<ProductDto> result = await _service.All();
            return result;
        }

        public async Task<ProductDto> GetProductByIdAsync(Guid id)
        {
            return await _service.GetProductById(id);
        }
    }
}
