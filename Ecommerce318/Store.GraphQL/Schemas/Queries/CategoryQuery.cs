﻿using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.GraphQL.Schemas.Queries
{
    [ExtendObjectType(Name = "Query")]//annotation

    public class CategoryQuery
    {
        private readonly ICategoryService _service;

        public CategoryQuery(ICategoryService service)
        {
            _service = service;
        }

        public async Task<IEnumerable<CategoryDto>> GetAllCategoryAsync()
        {
            IEnumerable<CategoryDto> result = await _service.All();
            return result;
        }

        public async Task<CategoryDto> GetCategoryByIdAsync(Guid id)
        {
            return await _service.GetCategoryById(id);
        }
    }
}
