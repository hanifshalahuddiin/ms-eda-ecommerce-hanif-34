﻿
using FluentValidation;
using FluentValidation.Results;
using Cart.Domain;
using Cart.Domain.Dtos;
using Cart.Domain.Services;

// [Route()], [ApiController], ControllerBase, [FromBody]
// IActionResult: { OK(), BadRequest(), NoContent() }
// [HttpGet], [HttpPost], [HttpPut]
using Microsoft.AspNetCore.Mvc;

namespace Cart.OpenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _service;
        private readonly ILogger<UserController> _logger;
        private IValidator<UserDto> _validator;

        public UserController(IUserService service, ILogger<UserController> logger, IValidator<UserDto> validator)
        {
            _service = service;
            _logger = logger;
            _validator = validator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _service.All());
        }

        //[HttpPost]
        //public async Task<IActionResult> Post([FromBody] UserDto payload, CancellationToken cancellationToken)
        //{
        //    try
        //    {
        //        ValidationResult result = await _validator.ValidateAsync(payload);
        //        if (!result.IsValid)
        //        {
        //            return BadRequest(result);
        //        }

        //        var dto = await _service.AddUser(payload);
        //        if (dto != null)
        //        {
        //            return Ok(dto);
        //        }
        //    }
        //    catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
        //    {
        //        _logger.LogWarning(ex.Message);
        //        throw;
        //    }
        //    return BadRequest();
        //}

        //[HttpPut]
        //public async Task<IActionResult> Put(Guid id, [FromBody] UserDto payload, CancellationToken cancellationToken)
        //{
        //    try
        //    {
        //        if (payload != null)
        //        {
        //            payload.Id = id;
        //            var isUpdated = await _service.UpdateUser(payload);
        //            if (isUpdated)
        //            {
        //                return Ok(isUpdated);
        //            }
        //        }
        //    }
        //    catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
        //    {
        //        _logger.LogWarning(ex.Message);
        //    }
        //    return NoContent();
        //}

        //[HttpPut("status")]
        //public async Task<IActionResult> Put(Guid id, UserStatusEnum status, CancellationToken cancellationToken)
        //{
        //    try
        //    {
        //        var isUpdated = await _service.UpdateUserStatus(id, status);
        //        if (isUpdated)
        //        {
        //            return Ok(isUpdated);
        //        }
        //    }
        //    catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
        //    {
        //        _logger.LogWarning(ex.Message);
        //    }
        //    return NoContent();
        //}

        //[HttpDelete]
        //public async Task<IActionResult> Delete(Guid id, CancellationToken cancellationToken)
        //{
        //    try
        //    {
        //        var isDeleted = await _service.DeleteUser(id);
        //        if (isDeleted)
        //            return Ok(isDeleted);
        //    }
        //    catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
        //    {
        //        _logger.LogWarning(ex.Message);
        //    }
        //    return NoContent();
        //}





        [HttpGet("by id")]
        public async Task<IActionResult> SelectAllById(Guid id, CancellationToken ct)
        {
            try
            {
                var getById = await _service.GetUserById(id);
                return Ok(getById);
            }
            catch (OperationCanceledException ex) when (ct.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        //[HttpPut("manual")]
        //public async Task<IActionResult> Put(Guid id, [FromBody] UserDtoManual manual, CancellationToken cancellationToken)
        //{
        //    try
        //    {
        //        var isUpdated = await _service.UpdateUserManual(id, manual);
        //        if (isUpdated)
        //        {
        //            return Ok(isUpdated);
        //        }
        //    }
        //    catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
        //    {
        //        _logger.LogWarning(ex.Message);
        //    }
        //    return NoContent();
        //}

        //[HttpPut("Enum")]
        //public async Task<IActionResult> Put(Guid id, UserTypeEnum type, CancellationToken cancellationToken)
        //{
        //    try
        //    {
        //        var isUpdated = await _service.UpdateUserEnum(id, type);
        //        if (isUpdated)
        //            return Ok(isUpdated);
        //    }
        //    catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
        //    {
        //        _logger.LogWarning(ex.Message);
        //    }
        //    return NoContent();
        //}
    }
}
