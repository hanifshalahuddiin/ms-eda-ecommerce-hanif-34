﻿using FluentValidation;
using FluentValidation.Results;
using Cart.Domain;
using Cart.Domain.Dtos;
using Cart.Domain.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Cart.OpenApi.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    //public class CartProductsController : ControllerBase
    //{
    //    private readonly ICartProductsService _service;
    //    private readonly ILogger<CartProductsController> _logger;
    //    private IValidator<CartProductsDto> _validator;

    //    public CartProductsController(ICartProductsService service, ILogger<CartProductsController> logger, IValidator<CartProductsDto> validator)
    //    {
    //        _service = service;
    //        _logger = logger;
    //        _validator = validator;
    //    }

    //    [HttpGet]
    //    public async Task<IActionResult> Get()
    //    {
    //        return Ok(await _service.All());
    //    }

    //    [HttpPost]
    //    public async Task<IActionResult> Post([FromBody] CartProductsDto payload, CancellationToken cancellationToken)
    //    {
    //        try
    //        {
    //            ValidationResult result = await _validator.ValidateAsync(payload);
    //            if (!result.IsValid)
    //            {
    //                return BadRequest(result.Errors);
    //            }

    //            var dto = await _service.AddCartProducts(payload);
    //            if (dto != null)
    //                return Ok(dto);
    //        }
    //        catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
    //        {
    //            _logger.LogWarning(ex.Message);
    //        }
    //        return BadRequest();
    //    }

    //    [HttpPut]
    //    public async Task<IActionResult> Put(Guid id, [FromBody] CartProductsDto payload, CancellationToken cancellationToken)
    //    {
    //        try
    //        {
    //            if (payload != null)
    //            {
    //                payload.Id = id;
    //                var isUpdated = await _service.UpdateCartProducts(payload);
    //                if (isUpdated)
    //                    return Ok(isUpdated);
    //            }
    //        }
    //        catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
    //        {
    //            _logger.LogWarning(ex.Message);
    //        }
    //        return NoContent();
    //    }

    //    [HttpPut("status")]
    //    public async Task<IActionResult> Put(Guid id, LookUpStatusEnum status, CancellationToken cancellationToken)
    //    {
    //        try
    //        {
    //            var isUpdated = await _service.UpdateCartProductsStatus(id, status);
    //            if (isUpdated)
    //                return Ok(isUpdated);
    //        }
    //        catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
    //        {
    //            _logger.LogWarning(ex.Message);
    //        }
    //        return NoContent();
    //    }

        //[HttpPut("NoStatus")]
        //public async Task<IActionResult> Put(Guid id, [FromBody] AttributeNoStatusDto payload, CancellationToken cancellationToken)
        //{
        //    try
        //    {
        //        if (payload != null)
        //        {
        //            payload.Id = id;
        //            var isUpdated = await _service.UpdateAttribute(payload);
        //            if (isUpdated)
        //                return Ok(isUpdated);
        //        }
        //    }
        //    catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
        //    {
        //        _logger.LogWarning(ex.Message);
        //    }
        //    return NoContent();
        //}
    //}
}
