using FluentValidation;
using Framework.Kafka;
using Cart.Domain;
using Cart.Domain.MapProfile;
using Cart.Domain.Repositories;
using Cart.Domain.Services;
using Cart.Domain.Validations;
using Microsoft.EntityFrameworkCore;
using Framework.Core.Events;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("Cart_Db_Conn").Value);

    options
        .EnableSensitiveDataLogging(false)
        .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

builder.Services
    .AddScoped<IUserRepository, UserRepository>()
    .AddScoped<IUserService, UserService>()
    .AddScoped<ICartRepository, CartRepository>()
    .AddScoped<ICartService, CartService>();

builder.Services
    .AddValidatorsFromAssemblyContaining<UserValidator>()
    .AddValidatorsFromAssemblyContaining<CartValidator>();

builder.Services.AddControllers();

builder.Services.AddFromUser();

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});

//builder.Services.AddKafkaProducer();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddEventBus();
builder.Services.AddKafkaProducerAndConsumer();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
