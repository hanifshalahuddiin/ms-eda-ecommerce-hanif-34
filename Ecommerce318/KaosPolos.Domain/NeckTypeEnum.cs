﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace KaosPolos.Domain
{
    //internal class NeckTypeEnum
    //{
    //}

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum NeckTypeEnum
    {
        ONeck,
        VNeck,
        TurtleNeck,
        Polo
    }
}