﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace KaosPolos.Domain
{
    //internal class ColorTypeEnum
    //{
    //}

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum ColorTypeEnum
    {
        Black,
        White,
        Red,
        Blue,
        Yellow,
        Green,
        Purple,
        Orange,
        Grey
    }
}
