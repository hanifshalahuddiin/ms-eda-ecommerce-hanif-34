﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace KaosPolos.Domain
{
    //internal class KaosPolosStatusEnum
    //{
    //}

    [JsonConverter(typeof(JsonStringEnumConverter))]

    public enum KaosPolosStatusEnum
    {
        Discontinue,
        Continue
    }
}
