﻿using Microsoft.EntityFrameworkCore; //IEntityTypeConfiguration, ToTable
using Microsoft.EntityFrameworkCore.Metadata.Builders; //EntityTypeBuilder
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaosPolos.Domain.Entities.Configurations
{
    //internal class KaosPolosConfiguration
    //{
    //}

    public class AttributeConfiguration : IEntityTypeConfiguration<AttributeEntity>
    {
        public void Configure(EntityTypeBuilder<AttributeEntity> builder)
        {
            //throw new NotImplementedException();
            builder.ToTable("Attributes");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.NeckType).IsRequired();
            builder.Property(e => e.ColorType).IsRequired();
            builder.Property(e => e.SizeType).IsRequired();
            builder.Property(e => e.Status).IsRequired();
        }
    }
}
