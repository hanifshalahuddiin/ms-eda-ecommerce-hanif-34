﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaosPolos.Domain.Entities
{
    //internal class AttributeEntity
    //{
    //}

    public class AttributeEntity
    {
        public Guid Id { get; set; }
        public NeckTypeEnum NeckType { get; set; } = default!;
        public ColorTypeEnum ColorType { get; set; } = default!;
        public SizeTypeEnum SizeType { get; set; } = default!;
        public KaosPolosStatusEnum Status { get; set; } = default!;
        public DateTime Modified { get; internal set; } = DateTime.Now;
    }
}