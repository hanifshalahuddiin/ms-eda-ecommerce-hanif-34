﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace KaosPolos.Domain
{
    //internal class SizeTypeEnum
    //{
    //}

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum SizeTypeEnum
    {
        S,
        M,
        L,
        XL,
        XXL,
        Custom
    }
}
