﻿namespace Framework.Core.Events.Externals
{
    public class NulloExternalEventProducer : IExternalEventProducer
    {
        public Task Publish(IEventEnvelope @event, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
