﻿namespace User.Domain.EventEnvelopes.User
{
    public record UserStatusUpdated(
        Guid Id,
        string UserName,
        string Password,
        string FirstName,
        string LastName,
        string Email,
        UserStatusEnum Status,
        UserTypeEnum Type,
        DateTime Modified
    )
    {
        public static UserStatusUpdated UpdateStatus(
            Guid id,
            string UserName,
            string Password,
            string FirstName,
            string LastName,
            string Email,
            UserStatusEnum Status,
            UserTypeEnum Type,
            DateTime modified
        ) => new(id, UserName, Password, FirstName, LastName, Email, Status, Type, modified);
    }
}