﻿using AutoMapper;
using Framework.Auth;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using User.Domain.Dtos;
using User.Domain.Entities;
using User.Domain.EventEnvelopes.User;
using User.Domain.Repositories;

namespace User.Domain.Services
{
    public interface IUserService
    {
        Task<IEnumerable<UserDto>> All();
        Task<UserDto> GetUserById(Guid id);
        Task<UserDto> AddUser(UserDto dto);
        Task<bool> UpdateUser(UserDto dto);
        Task<bool> UpdateUserStatus(Guid id, UserStatusEnum status);
        Task<bool> DeleteUser(Guid id);

        Task<bool> UpdateUserManual(Guid id, UserDtoManual manual);
        Task<bool> UpdateUserEnum(Guid id, UserTypeEnum type);

        Task<LoginDto> Login(string userName, string password);
    }

    public class UserService : IUserService
    {
        private IUserRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public UserService(IUserRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<IEnumerable<UserDto>> All()
        {
            //throw new NotImplementedException();
            var result = _mapper.Map<IEnumerable<UserDto>>(await _repository.GetAll());
            return result;
        }

        public async Task<UserDto> GetUserById(Guid id)
        {
            //throw new NotImplementedException();
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<UserDto>(result);
                }
            }
            return new UserDto();
            //return null;
        }

        public async Task<UserDto> AddUser(UserDto dto)
        {
            //throw new NotImplementedException();
            if (dto != null)
            {
                dto.Status = UserStatusEnum.Inactive;
                dto.Password = Encryption.HashSha256(dto.Password);
                //dtoToEntity.Password = Encryption.HashSha256(dto.Password);

                var dtoToEntity = _mapper.Map<UserEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    //Record Kafka
                    var externalEvent = new EventEnvelope<UserCreated>(
                        UserCreated.Create(
                            entity.Id,
                            entity.UserName,
                            entity.Password,
                            entity.FirstName,
                            entity.LastName,
                            entity.Email,
                            entity.Status,
                            entity.Type,
                            entity.Modified
                        )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                    return _mapper.Map<UserDto>(entity);
                }
            }
            return new UserDto();
        }

        public async Task<bool> UpdateUser(UserDto dto)
        {
            //throw new NotImplementedException();
            if (dto != null)
            {
                var user = await _repository.GetById(dto.Id);
                if (user != null)
                {
                    dto.Status = user.Status;
                    var entity = await _repository.Update(_mapper.Map<UserEntity>(dto));
                    var result = await _repository.SaveChangesAsync();

                    //Record Kafka
                    var externalEvent = new EventEnvelope<UserUpdated>(
                        UserUpdated.Update(
                            entity.Id,
                            entity.UserName,
                            entity.Password,
                            entity.FirstName,
                            entity.LastName,
                            entity.Email,
                            entity.Status,
                            entity.Type,
                            entity.Modified
                        )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                    if (result > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> UpdateUserStatus(Guid id, UserStatusEnum status)
        {
            //throw new NotImplementedException();
            var user = await _repository.GetById(id);
            if (user != null)
            {
                user.Status = status;
                var entity = await _repository.Update(user);
                var result = await _repository.SaveChangesAsync();

                //Record Kafka
                var externalEvent = new EventEnvelope<UserStatusUpdated>(
                    UserStatusUpdated.UpdateStatus(
                        entity.Id,
                        entity.UserName,
                        entity.Password,
                        entity.FirstName,
                        entity.LastName,
                        entity.Email,
                        entity.Status,
                        entity.Type,
                        entity.Modified
                    )
                );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                if (result > 0)
                {
                    return true;
                }
            }
            return false;
        }

        //Delete
        public async Task<bool> DeleteUser(Guid id)
        {
            var user = await _repository.GetById(id);
            if (user != null)
            {
                user.Status = UserStatusEnum.Removed;
                var entity = await _repository.Delete(user);
                var result = await _repository.SaveChangesAsync();

                //Record Kafka
                var externalEvent = new EventEnvelope<UserDeleted>(
                    UserDeleted.Delete(
                        entity.Id,
                        entity.UserName,
                        entity.Password,
                        entity.FirstName,
                        entity.LastName,
                        entity.Email,
                        entity.Status,
                        entity.Type,
                        entity.Modified
                    )
                );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                if (result > 0) return true;
            }
            return false;
        }

        public async Task<LoginDto> Login(string userName, string password)
        {
            var entity = await _repository.Login(userName, password);
            if (entity != null)
            {
                LoginDto dto = _mapper.Map<LoginDto>(entity);
                dto.Roles.Add(entity.Type.ToString());
                return dto;
            }
            return null;
        }



        //update data manual

        public async Task<bool> UpdateUserManual(Guid id, UserDtoManual manual)
        {
            var user = await _repository.GetById(id);
            if (user != null)
            {
                user.UserName = manual.UserName;
                user.Password = manual.Password;
                user.FirstName = manual.LastName;
                user.LastName = manual.LastName;
                user.Email = manual.Email;
                var entity = await _repository.Update(_mapper.Map<UserEntity>(user));
                var result = await _repository.SaveChangesAsync();

                //Record Kafka
                var externalEvent = new EventEnvelope<UserUpdated>(
                    UserUpdated.Update(
                        entity.Id,
                        entity.UserName,
                        entity.Password,
                        entity.FirstName,
                        entity.LastName,
                        entity.Email,
                        entity.Status,
                        entity.Type,
                        entity.Modified
                    )
                );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                if (result > 0)
                {
                    return true;
                }
            }
            return false;
        }

        //Update Data Enum
        public async Task<bool> UpdateUserEnum(Guid id, UserTypeEnum type)
        {
            var user = await _repository.GetById(id);
            if (user != null)
            {
                user.Type = type;
                var entity = await _repository.Update(user);
                var result = await _repository.SaveChangesAsync();

                //Record Kafka
                var externalEvent = new EventEnvelope<UserUpdated>(
                    UserUpdated.Update(
                        entity.Id,
                        entity.UserName,
                        entity.Password,
                        entity.FirstName,
                        entity.LastName,
                        entity.Email,
                        entity.Status,
                        entity.Type,
                        entity.Modified
                    )
                );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                if (result > 0) return true;
            }
            
            return false;
        }
    }
}
