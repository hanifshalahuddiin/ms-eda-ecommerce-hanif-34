﻿using FluentValidation;
using User.Domain.Dtos;

namespace User.Domain.Validations
{
    public class UserValidator : AbstractValidator<UserDto>
    {
        public UserValidator()
        {
            RuleFor(x => x.UserName)
                    .NotNull().WithMessage("Username must not be empty");
            //RuleFor(x => x.Password)
            //        .NotNull().WithMessage("Password must not be blank")
            //        .MinimumLength(8).WithMessage("Password must not less than 8 characters")
            //        .MaximumLength(30).WithMessage("Password must less than 30 characters")
            //        .Matches("abcdefghijklmnopqrstuvwxyz").WithMessage("Password must contains at least 1 lower case")
            //        .Matches("ABCDEFGHIJKLMNOPQRSTUVWXYZ").WithMessage("Password must contains at least 1 upper case")
            //        .Matches("0123456789").WithMessage("Password must contains at least 1 number")
            //        .Matches("!@#$%^&*()").WithMessage("Password must contains at least 1 of { !@#$%^&*() }");
            RuleFor(x => x.FirstName)
                    .NotNull().WithMessage("First name must not be empty");
            //RuleFor(x => x.LastName)
            //        .NotNull().WithMessage("Last name must not be empty");
            //RuleFor(x => x.Email)
            //        .NotNull().WithMessage("Email must not be empty");
            //RuleFor(x => x.Status)
            //        .NotNull().WithMessage("Status must not be empty");
            //RuleFor(x => x.Type)
            //        .NotNull().WithMessage("Type must not be empty");
        }
    }
}