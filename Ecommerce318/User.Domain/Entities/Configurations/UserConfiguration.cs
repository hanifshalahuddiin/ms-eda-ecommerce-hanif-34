﻿using Microsoft.EntityFrameworkCore; //ToTable, IEntityTypeConfiguration
using Microsoft.EntityFrameworkCore.Metadata.Builders; //EntityTypeBuilder,

namespace User.Domain.Entities.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<UserEntity>
    {
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.ToTable("Users");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.UserName).HasMaxLength(50).IsRequired();
            builder.Property(e => e.Password).HasMaxLength(100).IsRequired();
            builder.Property(e => e.FirstName).HasMaxLength(50).IsRequired();
            builder.Property(e => e.LastName).HasMaxLength(50).IsRequired();
            builder.Property(e => e.Email).HasMaxLength(100).IsRequired();
            builder.Property(e => e.Status).IsRequired();
            builder.Property(e => e.Type).IsRequired();
        }
    }
}