﻿using User.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Framework.Auth;

namespace User.Domain.Repositories
{
    public interface IUserRepository
    {
        Task<int> GetCount();
        Task<IEnumerable<UserEntity>> GetAll();
        Task<IEnumerable<UserEntity>> GetPaged(int page, int size);
        Task<UserEntity> GetById(Guid id);
        Task<UserEntity> Add(UserEntity entity);
        Task<UserEntity> Update(UserEntity entity);
        Task<UserEntity> Delete(UserEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

        Task<UserEntity> Login(string userName, string password);
    }

    public class UserRepository : IUserRepository
    {
        protected readonly UserDbContext _context;

        public UserRepository(UserDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<IEnumerable<UserEntity>> GetAll()
        {
            //throw new NotImplementedException();
            var result = await _context.Set<UserEntity>().ToListAsync();
            return result;
        }

        public async Task<UserEntity> GetById(Guid id)
        {
            //throw new NotImplementedException();
            var result = await _context.Set<UserEntity>().FindAsync(id);
            return result;
        }

        public async Task<UserEntity> Add(UserEntity entity)
        {
            //throw new NotImplementedException();
            var result = _context.Set<UserEntity>().Add(entity);
            return entity;
        }

        public async Task<UserEntity> Update(UserEntity entity)
        {
            //throw new NotImplementedException();
            _context.Set<UserEntity>().Update(entity);
            return entity;
        }

        public async Task<UserEntity> Delete(UserEntity entity)
        {
            //throw new NotImplementedException();
            _context.Set<UserEntity>().Update(entity);
            return entity;
        }

        public Task<int> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<UserEntity>> GetPaged(int page, int size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            //throw new NotImplementedException();
            var result = await _context.SaveChangesAsync(cancellationToken);
            return result;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public async Task<UserEntity> Login(string userName, string password)
        {
            return await _context.Set<UserEntity>().FirstOrDefaultAsync(o => o.UserName == userName && o.Password == Encryption.HashSha256(password));
        }
    }
}
