﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization; //JsonConverter, JsonStringEnumConverter
using System.Threading.Tasks;

namespace User.Domain
{
    //internal class UserStatusEnum
    //{
    //}

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum UserStatusEnum
    {
        Active,
        Inactive,
        Removed
    }
}
