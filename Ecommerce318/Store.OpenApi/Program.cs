using FluentValidation;
using Framework.Core.Events;
using Framework.Kafka;
using Microsoft.EntityFrameworkCore;
using Store.Domain;
using Store.Domain.MapProfile;
using Store.Domain.Repositories;
using Store.Domain.Services;
using Store.Domain.Validations;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("Store_Db_Conn").Value);

    options
        .EnableSensitiveDataLogging(false)
        .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

builder.Services
    .AddScoped<IAttributeRepository, AttributeRepository>()
    .AddScoped<IAttributeService, AttributeService>()
    .AddScoped<ICategoryRepository, CategoryRepository>()
    .AddScoped<ICategoryService, CategoryService>()
    .AddScoped<IProductRepository, ProductRepository>()
    .AddScoped<IProductService, ProductService>();

builder.Services
    .AddValidatorsFromAssemblyContaining<AttributeValidator>()
    .AddValidatorsFromAssemblyContaining<CategoryValidator>()
    .AddValidatorsFromAssemblyContaining<ProductValidator>();

builder.Services.AddControllers();

//builder.Services.AddStore();
builder.Services.AddFromLookUp();

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});

//builder.Services.AddKafkaProducer();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//builder.Services.AddEventBus();
//builder.Services.AddKafkaConsumer();

builder.Services.AddEventBus();
builder.Services.AddKafkaProducerAndConsumer();

//builder.Services.AddRazorPages();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
//if (!app.Environment.IsDevelopment())
//{
//    app.UseExceptionHandler("/Error");
//    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
//    app.UseHsts();
//}

app.UseHttpsRedirection();
//app.UseStaticFiles();

//app.UseRouting();

app.UseAuthorization();

//app.MapRazorPages();
app.MapControllers();

app.Run();
