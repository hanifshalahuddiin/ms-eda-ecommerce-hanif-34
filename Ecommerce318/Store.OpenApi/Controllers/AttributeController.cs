﻿using FluentValidation;
using FluentValidation.Results;
using Store.Domain;
using Store.Domain.Dtos;
using Store.Domain.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Store.OpenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttributeController : ControllerBase
    {
        private readonly IAttributeService _service;
        private readonly ILogger<AttributeController> _logger;
        private IValidator<AttributeDto> _validator;

        public AttributeController(IAttributeService service, ILogger<AttributeController> logger, IValidator<AttributeDto> validator)
        {
            _service = service;
            _logger = logger;
            _validator = validator;
        }
        //private readonly ICategoryService _service;
        //private readonly ILogger<CategoryController> _logger;
        //private IValidator<CategoryDto> _validator;

        //public AttributeController(ICategoryService service, ILogger<CategoryController> logger, IValidator<CategoryDto> validator)
        //{
        //    _service = service;
        //    _logger = logger;
        //    _validator = validator;
        //}

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _service.All());
        }

        [HttpGet("ById")]
        public async Task<IActionResult> SelectAllById(Guid id, CancellationToken ct)
        {
            try
            {
                var getById = await _service.GetAttributeById(id);
                return Ok(getById);
            }
            catch (OperationCanceledException ex) when (ct.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        //[HttpPost]
        //public async Task<IActionResult> Post([FromBody] AttributeDto payload, CancellationToken cancellationToken)
        //{
        //    try
        //    {
        //        ValidationResult result = await _validator.ValidateAsync(payload);
        //        if (!result.IsValid)
        //        {
        //            return BadRequest(result);
        //        }

        //        var dto = await _service.AddAttribute(payload);
        //        if (dto != null)
        //            return Ok(dto);
        //    }
        //    catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
        //    {
        //        _logger.LogWarning(ex.Message);
        //    }
        //    return BadRequest();
        //}

        //[HttpPut]
        //public async Task<IActionResult> Put(Guid id, [FromBody] AttributeDto payload, CancellationToken cancellationToken)
        //{
        //    try
        //    {
        //        if (payload != null)
        //        {
        //            payload.Id = id;
        //            var isUpdated = await _service.UpdateAttribute(payload);
        //            if (isUpdated)
        //                return Ok(isUpdated);
        //        }
        //    }
        //    catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
        //    {
        //        _logger.LogWarning(ex.Message);
        //    }
        //    return NoContent();
        //}

        //[HttpPut("status")]
        //public async Task<IActionResult> Put(Guid id, LookUpStatusEnum status, CancellationToken cancellationToken)
        //{
        //    try
        //    {
        //        var isUpdated = await _service.UpdateAttributeStatus(id, status);
        //        if (isUpdated)
        //            return Ok(isUpdated);
        //    }
        //    catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
        //    {
        //        _logger.LogWarning(ex.Message);
        //    }
        //    return NoContent();
        //}
    }
}
