﻿using FluentValidation;
using FluentValidation.Results;
using Store.Domain;
using Store.Domain.Dtos;
using Store.Domain.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Store.OpenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _service;
        private readonly ILogger<ProductController> _logger;
        private IValidator<ProductDto> _validator;

        public ProductController(IProductService service, ILogger<ProductController> logger, IValidator<ProductDto> validator)
        {
            _service = service;
            _logger = logger;
            _validator = validator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _service.All());
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProductDto payload, CancellationToken cancellationToken)
        {
            try
            {
                ValidationResult result = await _validator.ValidateAsync(payload);
                if (!result.IsValid)
                {
                    return BadRequest(result);
                }

                var dto = await _service.AddProduct(payload);
                if (dto != null)
                    return Ok(dto);
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Put(Guid id, [FromBody] ProductDto payload, CancellationToken cancellationToken)
        {
            try
            {
                if (payload != null)
                {
                    payload.Id = id;
                    var isUpdated = await _service.UpdateProduct(payload);
                    if (isUpdated)
                        return Ok(isUpdated);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpPut("status")]
        public async Task<IActionResult> Put(Guid id, ProductStatusEnum status, CancellationToken cancellationToken)
        {
            try
            {
                var isUpdated = await _service.UpdateProductStatus(id, status);
                if (isUpdated)
                    return Ok(isUpdated);
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id, CancellationToken cancellationToken)
        {
            try
            {
                var isDeleted = await _service.DeleteProduct(id);
                if (isDeleted)
                    return Ok(isDeleted);
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }





        [HttpGet("ById")]
        public async Task<IActionResult> SelectAllById(Guid id, CancellationToken ct)
        {
            try
            {
                var getById = await _service.GetProductById(id);
                return Ok(getById);
            }
            catch (OperationCanceledException ex) when (ct.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpPut("manual")]
        public async Task<IActionResult> Put(Guid id, [FromBody] ProductDtoManual manual, CancellationToken cancellationToken)
        {
            try
            {
                var isUpdated = await _service.UpdateProductManual(id, manual);
                if (isUpdated)
                {
                    return Ok(isUpdated);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        //[HttpPut("Enum")]
        //public async Task<IActionResult> Put(Guid id, CategoryTypeEnum type, CancellationToken cancellationToken)
        //{
        //    try
        //    {
        //        var isUpdated = await _service.UpdateCategoryEnum(id, type);
        //        if (isUpdated)
        //            return Ok(isUpdated);
        //    }
        //    catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
        //    {
        //        _logger.LogWarning(ex.Message);
        //    }
        //    return NoContent();
        //}
    }
}
