﻿using AutoMapper;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using LookUp.Domain.Dtos;
using LookUp.Domain.Entities;
using LookUp.Domain.EventEnvelopes.Attribute;
using LookUp.Domain.Repositories;

namespace LookUp.Domain.Services
{
    public interface IAttributeService
    {
        Task<IEnumerable<AttributeDto>> All();
        Task<AttributeDto> GetAttributeById(Guid id);
        Task<AttributeDto> AddAttribute(AttributeDto dto);
        Task<bool> UpdateAttribute(AttributeDto dto);
        Task<bool> UpdateAttributeStatus(Guid id, LookUpStatusEnum status);
        Task<bool> DeleteAttribute(Guid id);
        //Task<bool> UpdateAttributeNew(AttributeDto dto);

        //Task<IEnumerable<AttributeDto>> GetAttributeActive(LookUpStatusEnum status);
        Task<bool> UpdateAttributeManual(Guid id, AttributeDtoManual manual);
        Task<bool> UpdateAttributeEnum(Guid id, LookUpStatusEnum status, AttributeTypeEnum type);

        //Task<bool> UpdateAttribute(AttributeNoStatusDto dto);
    }

    public class AttributeService : IAttributeService
    {
        private IAttributeRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public AttributeService(IAttributeRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        //Select Data
        public async Task<IEnumerable<AttributeDto>> All()
        {
            return _mapper.Map<IEnumerable<AttributeDto>>(await _repository.GetAll());
        }

        public async Task<AttributeDto> GetAttributeById(Guid id)
        {
            if (id != Guid.Empty)
            {
                //await
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<AttributeDto>(result);
                }
            }
            return new AttributeDto();
            //return null;
        }

        //Insert Data
        public async Task<AttributeDto> AddAttribute(AttributeDto dto)
        {
            if (dto != null)
            {
                dto.Status = LookUpStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<AttributeEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {

                //Record Kafka
                    var externalEvent = new EventEnvelope<AttributeCreated>(
                        AttributeCreated.Create(
                            entity.Id,
                            entity.Type,
                            entity.Unit,
                            entity.Status,
                            entity.Modified
                        )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                    return _mapper.Map<AttributeDto>(entity);
                }
            }
            return new AttributeDto();
        }

    //Update Data
        public async Task<bool> UpdateAttribute(AttributeDto dto)
        {
            if (dto != null)
            {
                var attribute = await _repository.GetById(dto.Id);
                if (attribute != null)
                {
                    dto.Status = attribute.Status;
                    var entity = await _repository.Update(_mapper.Map<AttributeEntity>(dto));
                    var result = await _repository.SaveChangesAsync();

                //Record Kafka
                    var externalEvent = new EventEnvelope<AttributeUpdated>(
                        AttributeUpdated.Update(
                            entity.Id,
                            entity.Type,
                            entity.Unit,
                            entity.Status,
                            entity.Modified
                        )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                    if (result > 0) return true;
                }
            }
            return false;
        }

        public async Task<bool> UpdateAttributeStatus(Guid id, LookUpStatusEnum status)
        {
            var attribute = await _repository.GetById(id);
            if (attribute != null)
            {
                attribute.Status = status;
                var entity = await _repository.Update(attribute);
                var result = await _repository.SaveChangesAsync();

                //Record Kafka
                var externalEvent = new EventEnvelope<AttributeStatusUpdated>(
                    AttributeStatusUpdated.UpdateStatus(
                        entity.Id,
                        entity.Type,
                        entity.Unit,
                        entity.Status,
                        entity.Modified
                    )
                );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                if (result > 0) return true;
            }
            return false;
        }

        //Delete
        public async Task<bool> DeleteAttribute(Guid id)
        {
            var attribute = await _repository.GetById(id);
            if (attribute != null)
            {
                attribute.Status = LookUpStatusEnum.Removed;
                var entity = await _repository.Delete(attribute);
                var result = await _repository.SaveChangesAsync();

                //Record Kafka
                var externalEvent = new EventEnvelope<AttributeDeleted>(
                    AttributeDeleted.Delete(
                        entity.Id,
                        entity.Type,
                        entity.Unit,
                        entity.Status,
                        entity.Modified
                    )
                );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                if (result > 0) return true;
            }
            return false;
        }





        //Select Data
        //public async Task<IEnumerable<AttributeDto>> GetAttributeActive(LookUpStatusEnum status)
        //{
        //    return _mapper.Map<IEnumerable<AttributeDto>>(await _repository.GetAll());
        //}

        //Update Data
        public async Task<bool> UpdateAttributeManual(Guid id, AttributeDtoManual manual)
        {
            if (id != null)
            {
                var attribute = await _repository.GetById(id);
                if (attribute != null)
                {
                    attribute.Unit = manual.Unit;
                    var entity = await _repository.Update(_mapper.Map<AttributeEntity>(attribute));
                    var result = await _repository.SaveChangesAsync();

                    //Record Kafka
                    var externalEvent = new EventEnvelope<AttributeUpdated>(
                        AttributeUpdated.Update(
                            entity.Id,
                            entity.Type,
                            entity.Unit,
                            entity.Status,
                            entity.Modified
                        )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                    if (result > 0) return true;
                }
            }
            return false;
        }

        //Update Data
        public async Task<bool> UpdateAttributeEnum(Guid id, LookUpStatusEnum status, AttributeTypeEnum type)
        {
            if (id != null)
            {
                var attribute = await _repository.GetById(id);
                if (attribute != null)
                {
                    attribute.Status = status;
                    attribute.Type = type;
                    var entity = await _repository.Update(attribute);
                    var result = await _repository.SaveChangesAsync();

                    //Record Kafka
                    var externalEvent = new EventEnvelope<AttributeUpdated>(
                        AttributeUpdated.Update(
                            entity.Id,
                            entity.Type,
                            entity.Unit,
                            entity.Status,
                            entity.Modified
                        )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                    if (result > 0) return true;
                }
            }
            return false;
        }



        //Update Data
        //public async Task<bool> UpdateAttributeNew(Guid id, AttributeDtoOption option)
        //{
        //    if (id != null)
        //    {
        //        var attribute = await _repository.GetById(id);
        //        if (attribute != null)
        //        {
        //            attribute.Type = option.Type;
        //            var entity = await _repository.Update(_mapper.Map<AttributeEntity>(attribute));
        //            var result = await _repository.SaveChangesAsync();

        //            //Record Kafka
        //            var externalEvent = new EventEnvelope<AttributeUpdated>(
        //                AttributeUpdated.Update(
        //                    entity.Id,
        //                    entity.Type,
        //                    entity.Unit,
        //                    entity.Status,
        //                    entity.Modified
        //                )
        //            );
        //            await _externalEventProducer.Publish(externalEvent, new CancellationToken());

        //            if (result > 0) return true;
        //        }
        //    }
        //    return false;
        //}



        //public async Task<bool> UpdateAttribute(AttributeNoStatusDto dto)
        //{
        //    if (dto != null)
        //    {
        //        var attribute = await _repository.GetById(dto.Id);
        //        if (attribute != null)
        //        {
        //            var updateDto = _mapper.Map<AttributeNoStatusDto>(attribute);
        //            var entity = await _repository.Update(_mapper.Map<AttributeEntity>(updateDto));
        //            var result = await _repository.SaveChangesAsync();

        //            //Record Kafka
        //            var externalEvent = new EventEnvelope<AttributeUpdated>(
        //                AttributeUpdated.Update(
        //                    entity.Id,
        //                    entity.Type,
        //                    entity.Unit,
        //                    entity.Status,
        //                    entity.Modified
        //                )
        //            );
        //            await _externalEventProducer.Publish(externalEvent, new CancellationToken());

        //            if (result > 0) return true;
        //        }
        //    }
        //    return false;
        //}
    }
}
