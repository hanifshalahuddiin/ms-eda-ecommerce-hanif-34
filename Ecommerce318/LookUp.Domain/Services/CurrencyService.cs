﻿using AutoMapper;
using LookUp.Domain.Dtos;
using LookUp.Domain.Repositories;
using LookUp.Domain.Entities;
using Framework.Core.Events.Externals;
using LookUp.Domain.EventEnvelopes.Currency;
using Framework.Core.Events;

namespace LookUp.Domain.Services
{
    public interface ICurrencyService
    {
        Task<IEnumerable<CurrencyDto>> All();
        Task<CurrencyDto> GetCurrencyById(Guid id);
        Task<CurrencyDto> AddCurrency(CurrencyDto dto);
        Task<bool> UpdateCurrency(CurrencyDto dto);
        Task<bool> UpdateCurrencyStatus(Guid id, LookUpStatusEnum status);

        Task<bool> DeleteCurrency(Guid id);
        Task<bool> UpdateCurrencyManual(Guid id, CurrencyDtoManual manual);
        Task<bool> UpdateCurrencyEnum(Guid id, CurrencyNameEnum name, CurrencyCodeEnum code, CurrencySymbolEnum symbol);
    }

    public class CurrencyService : ICurrencyService
    {
        private ICurrencyRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public CurrencyService(ICurrencyRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<IEnumerable<CurrencyDto>> All()
        {
            var result = _mapper.Map<IEnumerable<CurrencyDto>>(await _repository.GetAll());
            return result;
        }

        public async Task<CurrencyDto> GetCurrencyById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<CurrencyDto>(result);
                }
            }
            //return null;
            return new CurrencyDto();
        }

        public async Task<CurrencyDto> AddCurrency(CurrencyDto dto)
        {
            if (dto != null)
            {
                dto.Status = LookUpStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<CurrencyEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    //Record Kafka
                    var externalEvent = new EventEnvelope<CurrencyDeleted>(
                        CurrencyDeleted.Delete(
                            entity.Id,
                            entity.Name,
                            entity.Code,
                            entity.Symbol,
                            entity.Status,
                            entity.Modified
                        )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                    return _mapper.Map<CurrencyDto>(entity);
                }
            }
            return new CurrencyDto();
        }

        public async Task<bool> UpdateCurrency(CurrencyDto dto)
        {
            if (dto != null)
            {
                var currency = await _repository.GetById(dto.Id);
                if (currency != null)
                {
                    dto.Status = currency.Status;
                    var entity = await _repository.Update(_mapper.Map<CurrencyEntity>(dto));
                    var result = await _repository.SaveChangesAsync();

                    //Record Kafka
                    var externalEvent = new EventEnvelope<CurrencyUpdated>(
                        CurrencyUpdated.Update(
                            entity.Id,
                            entity.Name,
                            entity.Code,
                            entity.Symbol,
                            entity.Status,
                            entity.Modified
                        )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                    if (result > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> UpdateCurrencyStatus(Guid id, LookUpStatusEnum status)
        {
            var currency = await _repository.GetById(id);
            if (currency != null)
            {
                currency.Status = status;
                var entity = await _repository.Update(currency);
                var result = await _repository.SaveChangesAsync();

                //Record Kafka
                var externalEvent = new EventEnvelope<CurrencyStatusUpdated>(
                    CurrencyStatusUpdated.UpdateStatus(
                        entity.Id,
                        entity.Name,
                        entity.Code,
                        entity.Symbol,
                        entity.Status,
                        entity.Modified
                    )
                );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                if (result > 0)
                {
                    return true;
                }
            }
            return false;
        }


        //Delete
        public async Task<bool> DeleteCurrency(Guid id)
        {
            var currency = await _repository.GetById(id);
            if (currency != null)
            {
                currency.Status = LookUpStatusEnum.Removed;
                var entity = await _repository.Delete(currency);
                var result = await _repository.SaveChangesAsync();

                //Record Kafka
                var externalEvent = new EventEnvelope<CurrencyDeleted>(
                    CurrencyDeleted.Delete(
                        entity.Id,
                        entity.Name,
                        entity.Code,
                        entity.Symbol,
                        entity.Status,
                        entity.Modified
                    )
                );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                if (result > 0)
                {
                    return true;
                }
            }
            return false;
        }

        //Update Data
        public async Task<bool> UpdateCurrencyManual(Guid id, CurrencyDtoManual manual)
        {
            var currency = await _repository.GetById(id);
            if (currency != null)
            {
                currency.Name = manual.Name;
                currency.Code = manual.Code;
                currency.Symbol = manual.Symbol;
                var entity = await _repository.Update(_mapper.Map<CurrencyEntity>(currency));
                var result = await _repository.SaveChangesAsync();

                //Record Kafka
                var externalEvent = new EventEnvelope<CurrencyUpdated>(
                    CurrencyUpdated.Update(
                        entity.Id,
                        entity.Name,
                        entity.Code,
                        entity.Symbol,
                        entity.Status,
                        entity.Modified
                    )
                );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                if (result > 0)
                {
                    return true;
                }
            }
            return false;
        }

        //Update Data
        public async Task<bool> UpdateCurrencyEnum(Guid id, CurrencyNameEnum name, CurrencyCodeEnum code, CurrencySymbolEnum symbol)
        {
            //var currency = await _repository.GetById(id);
            //if (currency != null)
            //{
            //    currency.Code = code;
            //    currency.Symbol = symbol;
            //    var entity = await _repository.Update(currency);
            //    var result = await _repository.SaveChangesAsync();

            //    //Record Kafka
            //    var externalEvent = new EventEnvelope<CurrencyUpdated>(
            //        CurrencyUpdated.Update(
            //            entity.Id,
            //            entity.Name,
            //            entity.Code,
            //            entity.Symbol,
            //            entity.Status,
            //            entity.Modified
            //        )
            //    );
            //    await _externalEventProducer.Publish(externalEvent, new CancellationToken());

            //    if (result > 0)
            //    {
            //        return true;
            //    }
            //}
            return false;
        }
    }
}
