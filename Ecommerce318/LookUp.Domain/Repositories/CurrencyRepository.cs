﻿using LookUp.Domain.Entities; //CurrencyEntity, LookUpDbContext
using Microsoft.EntityFrameworkCore; //ToListAsync

namespace LookUp.Domain.Repositories
{
    public interface ICurrencyRepository
    {
        Task<int> GetCount();
        Task<IEnumerable<CurrencyEntity>> GetAll();
        Task<IEnumerable<CurrencyEntity>> GetPaged(int page, int pageSize);
        Task<CurrencyEntity> GetById(Guid id);
        Task<CurrencyEntity> Add(CurrencyEntity entity);
        Task<CurrencyEntity> Update(CurrencyEntity entity);
        Task<CurrencyEntity> Delete(CurrencyEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }

    public class CurrencyRepository : ICurrencyRepository
    {
        protected readonly LookUpDbContext _context;

        public CurrencyRepository(LookUpDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<IEnumerable<CurrencyEntity>> GetAll()
        {
            var result = await _context.Set<CurrencyEntity>().ToListAsync();
            return result;
        }

        public async Task<CurrencyEntity> GetById(Guid id)
        {
            var result = await _context.Set<CurrencyEntity>().FindAsync(id);
            return result;
        }

        public async Task<CurrencyEntity> Add(CurrencyEntity entity)
        {
            _context.Set<CurrencyEntity>().Add(entity);
            return entity;
        }

        public async Task<CurrencyEntity> Update(CurrencyEntity entity)
        {
            _context.Set<CurrencyEntity>().Update(entity);
            return entity;
        }

        public async Task<CurrencyEntity> Delete(CurrencyEntity entity)
        {
            _context.Set<CurrencyEntity>().Update(entity);
            return entity;
        }

        public async Task<int> SaveChangesAsync(CancellationToken ct = default)
        {
            var result = await _context.SaveChangesAsync(ct);
            return result;
        }

        public Task<int> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<CurrencyEntity>> GetPaged(int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
