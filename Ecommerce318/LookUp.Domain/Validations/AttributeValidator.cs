﻿using FluentValidation;
using LookUp.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Validations
{
    public class AttributeValidator : AbstractValidator<AttributeDto>
    {
        public AttributeValidator()
        {
            //RuleFor(x => x.Name).NotEmpty();
            //RuleFor(x => x.Name).NotNull().WithMessage("Name must not be null");
            //RuleFor(x => x.Unit).MinimumLength(5).WithMessage("Description must not less than 5 characters");
            //RuleFor(x => x.Type).IsInEnum().NotEmpty();
            //RuleFor(x => x.Unit).Length(3, 20).WithMessage("Description must not less than 3 and not exceed 20 characters");
            RuleFor(x => x.Unit).MinimumLength(3).WithMessage("Description must not less than 3 characters").MaximumLength(20).WithMessage("Description must not exceed 20 characters");
        }
    }

}