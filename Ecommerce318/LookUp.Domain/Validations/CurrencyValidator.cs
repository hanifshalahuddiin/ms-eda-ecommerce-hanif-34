﻿using FluentValidation;
using LookUp.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Validations
{
    public class CurrencyValidator : AbstractValidator<CurrencyDto>
    {
        public CurrencyValidator()
        {
            //RuleFor(x => x.Name).NotEmpty();
            //RuleFor(x => x.Name).NotNull().WithMessage("Name must not be null");
            //RuleFor(x => x.Unit).MinimumLength(5).WithMessage("Description must not less than 5 characters");
            //RuleFor(x => x.Type).IsInEnum().NotEmpty();
            RuleFor(x => x.Name).Length(3, 20).WithMessage("Description must not less than 3 and not exceed 20 characters");
            //RuleFor(x => x.Name).IsInEnum().NotEmpty();
            //RuleFor(x => x.Code).Length(1, 3).WithMessage("Code must not exceed 3 characters");
            RuleFor(x => x.Code).MinimumLength(1).WithMessage("Code must not lest than 1 character").MaximumLength(3).WithMessage("Code must not exceed 3 characters");
            //RuleFor(x => x.Code).IsInEnum().NotEmpty();
            RuleFor(x => x.Symbol).Length(1, 3).WithMessage("Symbol must be 1-3 characters");
            //RuleFor(x => x.Symbol).IsInEnum().NotEmpty();
        }
    }
}