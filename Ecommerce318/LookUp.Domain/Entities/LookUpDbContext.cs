﻿using LookUp.Domain.Entities.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace LookUp.Domain.Entities
{
    public class LookUpDbContext: DbContext
    {
        public LookUpDbContext(DbContextOptions<LookUpDbContext> options): base(options)
        {

        }

        public DbSet<AttributeEntity> Attributes { get; set; }
        public DbSet<CurrencyEntity> Currencies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new AttributeConfiguration());
            modelBuilder.ApplyConfiguration(new CurrencyConfiguration());
        }

        //public static DbContextOptions<LookUpDbContext> OnConfigure()
        //{
        //    var optionsBuilder = new DbContextOptionsBuilder<LookUpDbContext>();
        //    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
        //    optionsBuilder.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("LookUp_Db_Conn").Value);

        //    return optionsBuilder.Options;
        //}
    }
}
