﻿
namespace LookUp.Domain.Dtos
{
    public class AttributeDto
    {
        public Guid Id { get; set; }
        public AttributeTypeEnum Type { get; set; }
        public string Unit { get; set; }
        public LookUpStatusEnum Status { get; internal set; }
    }

    public class AttributeDtoManual
    {
        public string Unit { get; set; }
    }

    public class AttributeDtoOption
    {
        public Guid Id { get; set; }
        public AttributeTypeEnum Type { get; set; }
    }


    public class AttributeNoStatusDto
    {
        public Guid Id { get; set; }
        public AttributeTypeEnum Type { get; set; }
        public string Unit { get; set; }
        //public LookUpStatusEnum Status { get; internal set; }
    }
}
