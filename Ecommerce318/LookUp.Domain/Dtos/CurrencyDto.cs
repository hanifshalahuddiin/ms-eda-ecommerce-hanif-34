﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Dtos
{
    public class CurrencyDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Symbol { get; set; }
        //public CurrencyNameEnum Name { get; set; }
        //public CurrencyCodeEnum Code { get; set; }
        //public CurrencySymbolEnum Symbol { get; set; }
        public LookUpStatusEnum Status { get; internal set; }
    }

    public class CurrencyDtoManual
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Symbol { get; set; }
    }

    public class CurrencyDtoEnum
    {
        public CurrencyNameEnum Name { get; set; }
        public CurrencyCodeEnum Code { get; set; }
        public CurrencySymbolEnum Symbol { get; set; }
    }
}
