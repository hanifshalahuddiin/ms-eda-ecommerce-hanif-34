﻿namespace LookUp.Domain.EventEnvelopes.Currency
{
    public record CurrencyCreated(
        Guid Id,
        string Name,
        string Code,
        string Symbol,
        LookUpStatusEnum Status,
        DateTime Modified
    )
    {
        public static CurrencyCreated Create(
            Guid Id,
            string Name,
            string Code,
            string Symbol,
            LookUpStatusEnum Status,
            DateTime Modified
        ) => new(Id, Name, Code, Symbol, Status, Modified);
    }
}