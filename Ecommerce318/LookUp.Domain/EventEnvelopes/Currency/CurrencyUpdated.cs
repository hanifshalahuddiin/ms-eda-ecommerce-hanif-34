﻿namespace LookUp.Domain.EventEnvelopes.Currency
{
    public record CurrencyUpdated(
        Guid Id,
        string Name,
        string Code,
        string Symbol,
        LookUpStatusEnum Status,
        DateTime Modified
    )
    {
        public static CurrencyUpdated Update(
            Guid Id,
            string Name,
            string Code,
            string Symbol,
            LookUpStatusEnum Status,
            DateTime Modified
        ) => new(Id, Name, Code, Symbol, Status, Modified);
    }
}