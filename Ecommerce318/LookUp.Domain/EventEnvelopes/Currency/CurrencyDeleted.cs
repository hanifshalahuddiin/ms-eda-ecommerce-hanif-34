﻿namespace LookUp.Domain.EventEnvelopes.Currency
{
    public record CurrencyDeleted(
        Guid Id,
        string Name,
        string Code,
        string Symbol,
        LookUpStatusEnum Status,
        DateTime Modified
    )
    {
        public static CurrencyDeleted Delete(
            Guid Id,
            string Name,
            string Code,
            string Symbol,
            LookUpStatusEnum Status,
            DateTime Modified
        ) => new(Id, Name, Code, Symbol, Status, Modified);
    }
}