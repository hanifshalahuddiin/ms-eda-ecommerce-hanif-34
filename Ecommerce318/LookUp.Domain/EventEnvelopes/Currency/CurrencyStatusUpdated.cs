﻿namespace LookUp.Domain.EventEnvelopes.Currency
{
    public record CurrencyStatusUpdated(
        Guid Id,
        string Name,
        string Code,
        string Symbol,
        LookUpStatusEnum Status,
        DateTime Modified
    )
    {
        public static CurrencyStatusUpdated UpdateStatus(
            Guid Id,
            string Name,
            string Code,
            string Symbol,
            LookUpStatusEnum Status,
            DateTime Modified
        ) => new(Id, Name, Code, Symbol, Status, Modified);
    }
}