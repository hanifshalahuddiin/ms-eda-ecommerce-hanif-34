﻿namespace LookUp.Domain.EventEnvelopes.Attribute
{
    public record AttributeUpdated(
        Guid Id,
        AttributeTypeEnum Type,
        string Unit,
        LookUpStatusEnum Status,
        DateTime Modified
    )
    {
        public static AttributeUpdated Update(
            Guid id,
            AttributeTypeEnum type,
            string unit,
            LookUpStatusEnum status,
            DateTime modified
        ) => new(id, type, unit, status, modified);
    }
}
