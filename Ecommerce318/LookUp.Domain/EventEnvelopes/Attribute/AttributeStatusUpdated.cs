﻿namespace LookUp.Domain.EventEnvelopes.Attribute
{
    public record AttributeStatusUpdated(
        Guid Id,
        AttributeTypeEnum Type,
        string Unit,
        LookUpStatusEnum Status,
        DateTime Modified
    )
    {
        public static AttributeStatusUpdated UpdateStatus(
            Guid id,
            AttributeTypeEnum type,
            string unit,
            LookUpStatusEnum status,
            DateTime modified
        ) => new(id, type, unit, status, modified);
    }
}
