﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
namespace LookUp.Domain
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum CurrencySymbolEnum
    {
        Rp,
        S
    }
}
