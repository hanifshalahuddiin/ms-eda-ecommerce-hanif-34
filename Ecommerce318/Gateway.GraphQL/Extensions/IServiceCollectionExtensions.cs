﻿namespace Gateway.GraphQL.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public const string Users = "UserService";
        public const string LookUps = "LookUpService";
        //public const string Stores = "StoreService";
        //public const string Stores = "CartService";
        //public const string Stores = "PaymentService";

        public static IServiceCollection AddHttpClientServices(this IServiceCollection services)
        {
            services.AddHttpClient(Users, c => c.BaseAddress = new Uri("https://localhost:7097/graphql"));
            services.AddHttpClient(LookUps, c => c.BaseAddress = new Uri("https://localhost:7022/graphql"));
            //services.AddHttpClient(Stores, c => c.BaseAddress = new Uri("https://localhost:7093/graphql"));
            //services.AddHttpClient(Carts, c => c.BaseAddress = new Uri("https://localhost:7071/graphql"));
            //services.AddHttpClient(Payments, c => c.BaseAddress = new Uri("https://localhost:7166/graphql"));

            services
                .AddGraphQLServer()
                .AddRemoteSchema(Users)
                .AddRemoteSchema(LookUps);
                //.AddRemoteSchema(Stores)
                //.AddRemoteSchema(Carts)
                //.AddRemoteSchema(Payments);

            return services;
        }
    }
}
