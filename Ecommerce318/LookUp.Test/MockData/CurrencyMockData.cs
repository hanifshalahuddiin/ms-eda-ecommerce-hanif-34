﻿using LookUp.Domain;
using LookUp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Test.MockData
{
    public class CurrencyMockData
    {
        public static List<CurrencyEntity> GetCurrencies()
        {
            return new List<CurrencyEntity>
            {
                new CurrencyEntity
                {
                    Id = new Guid("69C03A26-9EBD-474D-96DA-6B1880137C39"),
                    Name = "IndonesiaRupiah",
                    Code = "IDR",
                    Symbol = "Rp",
                    Status = LookUpStatusEnum.Active
                },
                new CurrencyEntity
                {
                    Id = new Guid("3DEC6705-B7F9-4D85-9081-63E01DD8C9A4"),
                    Name = "IndonesiaRupiah",
                    Code = "IDR",
                    Symbol = "Rp",
                    Status = LookUpStatusEnum.Active
                }


            };
        }
    }
}
