﻿using FluentAssertions;
using LookUp.Domain.Entities;
using LookUp.Domain.Repositories;
using LookUp.Test.MockData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace LookUp.Test.Domain.Repository
{
    public class TestCurrencyRepository : IDisposable
    {
        protected readonly LookUpDbContext _context;
        private readonly ITestOutputHelper _output;

        public TestCurrencyRepository(ITestOutputHelper output)
        {
            var options = new DbContextOptionsBuilder<LookUpDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            _context = new LookUpDbContext(options);
            _context.Database.EnsureCreated();

            _output = output;
        }

        [Fact]
        public async Task GetAllAsync_ReturnCurrenciesCollection()
        {
            //Arrange
            _context.Currencies.AddRange(CurrencyMockData.GetCurrencies());
            _context.SaveChanges();

            var repo = new CurrencyRepository(_context);
            //Action
            var result = await repo.GetAll();
            var count = CurrencyMockData.GetCurrencies().Count();
            _output.WriteLine("Count : {0}", count);
            //Assert
            result.Should().HaveCount(2);
        }


        [Fact]
        public async Task GetGetById_ReturnCurrenciesCollection()
        {
            //Arrange
            _context.Currencies.AddRange(CurrencyMockData.GetCurrencies());
            _context.SaveChanges();

            var repo = new CurrencyRepository(_context);

            //Action
            var result = await repo.GetById(new Guid("69C03A26-9EBD-474D-96DA-6B1880137C39"));
            //var result = await repo.GetAll();
            //var count = CurrencyMockData.GetCurrencies().Count();
            //_output.WriteLine("Count : {0}", count);

            //Assert
            //result.Should().HaveCount(2);
            result.Name.Should().Be("RinggitMalaysia");
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}
