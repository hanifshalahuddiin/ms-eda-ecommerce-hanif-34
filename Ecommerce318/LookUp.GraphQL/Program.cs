using Framework.Kafka;
using LookUp.Domain;
using LookUp.Domain.MapProfile;
using LookUp.Domain.Repositories;
using LookUp.Domain.Services;
using LookUp.GraphQL.Schemas.Mutations;
using LookUp.GraphQL.Schemas.Queries;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("LookUp_Db_Conn").Value);

    options
        .EnableSensitiveDataLogging(false)
        .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});

builder.Services.AddKafkaProducer();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services
    .AddScoped<Query>()
    .AddScoped<AttributeQuery>()
    .AddScoped<CurrencyQuery>()
    .AddScoped<Mutation>()
    .AddScoped<AttributeMutation>()
    .AddScoped<CurrencyMutation>()
    .AddScoped<IAttributeRepository, AttributeRepository>()
    .AddScoped<IAttributeService, AttributeService>()
    .AddScoped<ICurrencyRepository, CurrencyRepository>()
    .AddScoped<ICurrencyService, CurrencyService>()
    .AddGraphQLServer()
    .AddQueryType<Query>()
    .AddTypeExtension<AttributeQuery>()
    .AddTypeExtension<CurrencyQuery>()
    .AddMutationType<Mutation>()
    .AddTypeExtension<AttributeMutation>()
    .AddTypeExtension<CurrencyMutation>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapGraphQL();

app.Run();
