﻿using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUp.GraphQL.Schemas.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class CurrencyQuery
    {
        private readonly ICurrencyService _service;

        public CurrencyQuery(ICurrencyService service)
        {
            _service = service;
        }

        public async Task<IEnumerable<CurrencyDto>> GetAllCurrencyAsync()
        {
            IEnumerable<CurrencyDto> result = await _service.All();
            return result;
        }

        public async Task<CurrencyDto> GetCurrencyByIdAsync(Guid id)
        {
            return await _service.GetCurrencyById(id);
        }
    }
}
