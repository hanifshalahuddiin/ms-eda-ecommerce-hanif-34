﻿using LookUp.Domain;

namespace LookUp.GraphQL.Schemas.Mutations
{
    public class AttributeTypeInput
    {
        public AttributeTypeEnum Type { get; set; }
        public string Unit { get; set; }
    }
}