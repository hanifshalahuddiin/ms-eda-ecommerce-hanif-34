﻿using LookUp.Domain;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUp.GraphQL.Schemas.Mutations
{
    [ExtendObjectType(Name = "Mutation")]
    public class CurrencyMutation
    {
        private readonly ICurrencyService _service;

        public CurrencyMutation(ICurrencyService service)
        {
            _service = service;
        }
        public async Task<CurrencyDto> AddCurrencyAsync(CurrencyTypeInput currency)
        {
            CurrencyDto dto = new CurrencyDto();
            dto.Code = currency.Code;
            dto.Name = currency.Name;
            dto.Symbol = currency.Symbol;
            var result = await _service.AddCurrency(dto);
            return result;
        }

        public async Task<CurrencyDto> EditCurrencyAsync(Guid id, CurrencyTypeInput currency)
        {
            CurrencyDto dto = new CurrencyDto();
            dto.Id = id;
            dto.Code = currency.Code;
            dto.Name = currency.Name;
            dto.Symbol = currency.Symbol;
            var result = await _service.UpdateCurrency(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Currency not found", "404"));
            }
            return dto;
        }

        public async Task<CurrencyDto> ChangeCurrencyStatusAsync(Guid id, LookUpStatusEnum status)
        {
            //AttributeDto dto = new AttributeDto();
            //dto.Id = id;
            //dto.Status = status;

            var result = await _service.UpdateCurrencyStatus(id, status);
            if (result)
            {
                return await _service.GetCurrencyById(id);
            }
            throw new GraphQLException(new Error("Currency not found", "404"));

            //return dto;
        }

        public async Task<CurrencyDto> RemoveCurrencyAsync(Guid id, LookUpStatusEnum status)
        {
            throw new GraphQLException(new Error("Currency not found", "404"));
            //var result
        }
    }
}
