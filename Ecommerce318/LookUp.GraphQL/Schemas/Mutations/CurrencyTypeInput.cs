﻿using LookUp.Domain;

namespace LookUp.GraphQL.Schemas.Mutations
{
    public class CurrencyTypeInput
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Symbol { get; set; }
        //public CurrencyCodeEnum Code { get; set; }
        //public CurrencyNameEnum Name { get; set; }
        //public CurrencySymbolEnum Symbol { get; set; }
    }
}